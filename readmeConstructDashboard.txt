//components to make dashboard
import NavBar from '../../components/NavBar'
import SideNav from '../../components/sidenav'
import dashboardStyle from '../../assets/dashboardStyle'
<div className={ dashboardStyle.completePage }>
                <NavBar/>
                <div className={ dashboardStyle.completeContent }>
                    <SideNav />
                    <div className = { dashboardStyle.contentPage }>
                        <div className="p-8">
                            <h1 className=" text-2xl text-gray-600 ">Opciones</h1>
                        </div>
                    </div>
                </div>
            </div>


<div className="container my-12 mx-auto px-4 md:px-12">
                <div className="flex flex-wrap justify-center items-center -mx-1 lg:-mx-4 md:mt-64">
                    <h1 className="md:mt-12 text-white font-bold text-2xl">Servicios</h1>
                </div>
                <div class="flex flex-wrap md:px-64 mt-12">
                    <div class="w-full md:px-32 p-2">
                        <div class="text-gray-700 text-center bg-gray-400 p-2">1</div>
                    </div>
                    <div class="w-full md:px-32 p-2">
                        <div class="text-gray-700 text-center bg-gray-400 p-2">1</div>
                    </div>
                    <div class="w-full md:w-1/3 p-2">
                        <div class="text-gray-700 text-center bg-gray-400 p-2">2</div>
                    </div>
                    <div class="w-full md:w-1/3 p-2">
                        <div class="text-gray-700 text-center bg-gray-400 p-2">3</div>
                    </div>
                    <div class="w-full md:w-1/3 p-2">
                        <div class="text-gray-700 text-center bg-gray-400 p-2">3</div>
                    </div>
                </div>
            </div>


//update a object with set state
this.state = {
  food: {
    sandwich: {
      capsicum: true,
      crackers: true,
      mayonnaise: true
    },
    pizza: {
      jalapeno: true,
      extraCheese: false
    }
  }
}

this.setState(prevState => ({
  food: {
    ...prevState.food,           // copy all other key-value pairs of food object
    pizza: {                     // specific object of food object
      ...prevState.food.pizza,   // copy all pizza key-value pairs
      extraCheese: true          // update value of specific key
    }
  }
}))