import es from './es'
import en from './en'
import dutch from './dth' 
class Translation {

    translate(langage, phareKey){
        switch(langage){
            case 'es':
                return es[phareKey] ? es[phareKey] : 'No se encontro la palabra'
            break;
            case 'en':
                return en[phareKey] ? en[phareKey] : 'We didnt found the word'
            break;
            case 'dutch':
                return dutch[phareKey] ? dutch[phareKey] : 'Geen vertaling gevonden'
            break;
            default:
                return 'Lenguaje no definido'
        }
    }

}

export default new Translation