export default {
    completePage :  'bg-gray-300 h-screen flex flex-col',
    completeContent : 'flex sm:flex flex-1 sm:flex-1 w-full',
    contentPage : 'bg-white h-screen overflow-scroll w-full bg-gray-200'
}