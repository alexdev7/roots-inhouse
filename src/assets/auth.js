import axios from "axios"
import global from '../api/global'
class Auth{

    user = ''
    pass = ''

    async login(){
        
        const user = await axios.post(global.BASE_URL+'auth', {
            user : this.user,
            pass : this.pass
        },{
            headers: {
                'Content-Type': 'application/json'
            }
        })

        return user
    }

    getId(){
        if(this.isAuthenticatedSeller()){
            const { id } = JSON.parse(localStorage.getItem('session') ) || JSON.parse(localStorage.getItem('session-seller') )
            return id
        }
        
        return false
    }

    logout(){

        localStorage.removeItem('session') || localStorage.removeItem('session-seller')
    }
    
    logoutSeller(){
        localStorage.removeItem('session-seller')
    }

    getToken(){
        const { token } = JSON.parse(localStorage.getItem('session') ) || JSON.parse(localStorage.getItem('session-seller') )
        return token 
    }

    isAuthenticated(){
        
        if( localStorage.getItem('session') || localStorage.getItem('session-seller')){
            return true
        }

        return false
    }

    isAuthenticatedSeller(){
        
        if(localStorage.getItem('session-seller')){
            return true
        }

        return false
    }

    forcedLogOut(){
        localStorage.removeItem('session') || localStorage.removeItem('session-seller')
        window.location.href='/login'
    }
}

export default new Auth()