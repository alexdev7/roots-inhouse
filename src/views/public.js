import React, {Component} from 'react'

import PublicHome from './public/home'
import Services from './public/services'
import Sections from './public/sections'
import OptionsServices from './public/options'
import Project from './public/project/project'

import {publicChangePage} from '../redux/actions/app.actions'
import { connect } from 'react-redux'

class Public extends Component{
    
    constructor(props){
        super(props)

    }
    publicChangePage = (number, parameter_selected) => {
        this.props.publicChangePage(number, parameter_selected)
    }
    
    renderView = () => {
        switch(this.props.page.init_public_page){
            case 0:
                return <PublicHome publicChangePage = { this.publicChangePage } services={1}/>
            case 1:
                return <Services publicChangePage = {this.publicChangePage} sections={2} home={0}/>
            case 2:
                return <Sections publicChangePage = {this.publicChangePage} services={1} />
            case 3:
                return <Project publicChangePage = {this.publicChangePage} home={0}/>
            default:
                return <PublicHome publicChangePage = { this.publicChangePage } services={1}/>
        }
    }

    render(){
        return this.renderView()
    }
}

const mapStateToProps = (state) => {
    return {
        page : state.page
    }
}

const dispatchStateToProps = {
    publicChangePage
}

export default connect(mapStateToProps,dispatchStateToProps)(Public)