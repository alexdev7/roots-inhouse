import React, {Component} from 'react'
class HomeDashboard extends Component {
    renderHome = () => {
        return (
            <div className="w-full h-full bg-white px-3 py-2 rounded shadow-md">
                <h1 className=" text-2xl text-gray-600 "> Inicio </h1>
                <h1>Bienvenido </h1>
            </div>
        )
    }
    render(){
        const { user } = JSON.parse( localStorage.getItem('session') ) || JSON.parse( localStorage.getItem('session-seller') )
        return (
            <div className="p-3">
                {this.renderHome()}
            </div>
        )
    }
}

export default HomeDashboard