import React, {Component} from 'react'
import NavBar from '../../components/NavBar'
import SideNav from '../../components/sidenav'
import DashboardStyle from '../../assets/dashboardStyle'

import HomeDashboard from '../../views/auth/home'
import SectionsDashboard from '../../views/auth/sections'
import ServiceDashboard from '../../views/auth/services'
import SellersDashboard from '../../views/auth/seller'
import CotizationDashboard from '../../views/auth/cotization'
import ProjectsDashboard from '../../views/auth/projects'

import { connect } from 'react-redux'
class Dashboard extends Component{
    state={

    }
    renderView = () => {
        switch(this.props.page.init_page){
            case 0:
                return <HomeDashboard/>
            case 3:
                return <SectionsDashboard/>
            case 4:
                return <ServiceDashboard/>
            case 5:
                return <SellersDashboard/>
            case 6:
                return <CotizationDashboard />
            case 7:
                return <ProjectsDashboard />
            default:
                return <HomeDashboard/>
        }
    }
    render(){
        return(
            <div className={ DashboardStyle.completePage }>
                <NavBar/>
                <div className={ DashboardStyle.completeContent }>
                    <SideNav/>
                    <div className={ DashboardStyle.contentPage }>
                        { this.renderView() }
                    </div>
                </div>
                
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        page : state.page
    }
}

export default connect(mapStateToProps)(Dashboard) 