import React, {Component} from 'react'
import Navbar from 'components/Breadcrumb'
import axios from 'axios'
import global from 'api/global'
import Spinner from '@material-ui/core/CircularProgress'
class ShowProjects extends Component{
    state = {
        projects : [],
        isLoading:true,
        search:''
    }
    async componentDidMount () {
        const {data} = await axios.get(global.BASE_URL+`proyecto`)
        
        this.setState({
            projects : data.proyectos,
            isLoading : false
        })
    }

    searchProyect = event => {
        this.setState({
            search : event.target.value
        })
    }

    renderProjects = (project, index) => {
            return (
                <div key={index} className="w-full p-2">
                    <div 
                    onClick={()=> this.props.changeProjectView(1, {
                        project:project
                    })} 
                    className="text-gray-700 hover:bg-blue-600 hover:text-white flex justify-between py-5 rounded bg-gray-400 p-2">
                        <div className="w-full">
                            <p className="font-bold">{project.nombre_proyecto}</p>
                        </div>
                        <div className="text-center w-full">
                            <p className="font-bold">{project.nombre_empresa}</p>
                        </div>
                        <div className="text-center w-full">
                            <div>
                                <button className="px-3 py-1 rounded text-white bg-black">ver proyecto</button>
                            </div>
                        </div>
                    </div>
                </div>
            )
    }
    render(){
        const {search} = this.state

        const filteredProjects = this.state.projects.filter( project => {
            return project.nombre_proyecto.toLowerCase().indexOf(search.toLowerCase()) !== -1 || project.nombre_empresa.toLowerCase().indexOf(search.toLowerCase()) !== -1
        })
        return(
            <div className="h-full w-full bg-white px-3 py-2 pb-10 rounded shadow-md">
                <Navbar>
                    <li><a onClick={()=>this.props.changeProjectView(this.props.show)} className="text-blue cursor-pointer font-bold">Proyectos</a></li>
                </Navbar>
                <div className="flex flex-wrap py-4">
                    <div className="w-full pb-4">
                        <div class="relative">
                            <input autoComplete="off" onChange={ (event)=> this.searchProyect(event) } className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-10 text-gray-700 leading-tight focus:outline-none" placeholder="busca algo ..." id="inline-full-name" type="text" /> 
                            <div className="pointer-events-none absolute inset-y-0 left-0 flex items-center px-2 text-gray-700">
                                <span className="material-icons text-gray-600">
                                search
                                </span>
                            </div>
                        </div>
                        {
                            this.state.isLoading ? 
                                <div className="text-center my-32">
                                    <Spinner color="inherit"/>
                                </div>
                            :
                            
                                <div class="flex mt-3 flex-wrap">
                                <div class="w-full p-2">
                                    <div class="text-gray-700 flex justify-between p-2">
                                        <div className="w-full">
                                            <p className="font-bold">Proyecto</p>
                                        </div>
                                        <div className="text-center w-full">
                                            <p className="font-bold">Empresa</p>
                                        </div>
                                        <div className="text-center w-full">
                                            <p className="font-bold">Acciones</p>
                                        </div>
                                        
                                    </div>
                                </div>
                                {
                                    filteredProjects.map((projects, index)=>{
                                        return this.renderProjects(projects, index)
                                    })
                                }
                                </div>
                        }
                    </div>
                </div>
            </div>
        )
    }
}

export default ShowProjects