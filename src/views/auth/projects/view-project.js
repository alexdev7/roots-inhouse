import React, {Component} from 'react'
import Navbar from 'components/Breadcrumb'
import {connect} from 'react-redux'
class ViewProject extends Component{
    render(){
        
        return (
            <div className="w-full h-full pb-6 bg-white px-3 py-2 rounded shadow-md">
                <Navbar>
                    <li><a onClick={()=>this.props.changeProjectView(this.props.show)} className="text-blue underline cursor-pointer font-bold">Proyectos</a></li>
                    <li>
                        <span className="mx-2">/</span>
                    </li>
                    <li><a className="text-blue cursor-pointer font-bold">{this.props.page.parameter_project.project.nombre_proyecto}</a></li>
                </Navbar>
                <div className="flex flex-wrap w-full bg-gray-200 my-2 rounded">
                    <div className="w-full p-2">
                        <div className="text-center rounded shadow-lg bg-white p-2">
                            <h1 className="font-bold text-lg">Descripción general</h1>
                            <p> {this.props.page.parameter_project.project.comentarios} </p>
                        </div>
                    </div>
                    <div className="w-1/2 p-2">
                        <div className="bg-white rounded shadow-md px-10 py-10">
                            <h1 className="font-bold">Información de cliente</h1>
                            <p>Email: {this.props.page.parameter_project.project.email}</p>
                            <p>Email: {this.props.page.parameter_project.project.nombre_completo}</p>
                        </div>
                    </div>
                    <div className="w-1/2 p-2">
                    <div className="bg-white rounded shadow-md px-10 py-10">
                            <h1 className="font-bold">Información empresarial</h1>
                            <p>Nombre: {this.props.page.parameter_project.project.nombre_empresa}</p>
                            <p>Tipo: {this.props.page.parameter_project.project.tipo_empresa}</p>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
const dispatchStateToProps = state => {
    return {
        page : state.page
    }
}
export default connect(dispatchStateToProps)(ViewProject)