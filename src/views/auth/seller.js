import React, {Component} from 'react'

import { changeSellerSection } from '../../redux/actions/app.actions'

import { connect } from 'react-redux'

import ListSeller from './sellers/sellers'
import AddSeller from './sellers/add-seler'


class Seller extends Component{

    changeSellerSection = (number) => {
        this.props.changeSellerSection(number)
    }

    renderView (){
        switch(this.props.page.sellers){
            case 0:
                return <ListSeller changeSellerSection = { this.changeSellerSection }  add = {1}/>  
            case 1:
                return <AddSeller changeSellerSection = {this.changeSellerSection} show = {0} />
            default :
                return <ListSeller changeSellerSection = { this.changeSellerSection }  add = {1}/>
            
        }
    }
    render()
    {
        return(
            <div className="p-3">
                { this.renderView() }
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        page : state.page
    }
}

const dispatchStateToProps = {
    changeSellerSection
}

export default connect(mapStateToProps, dispatchStateToProps)(Seller) 