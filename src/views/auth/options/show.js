import React, {Component} from 'react'
import axios from 'axios'
import global from 'api/global'
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css';
import { ToastContainer, toast } from 'react-toastify'
import auth from 'assets/auth'
class ShowOptions extends Component{
    state={
        options:[],
        loading:false,
    }
    async componentDidMount(){
        const res = await axios.get(global.BASE_URL+'opciones')
        this.setState({
            options : res.data.opciones
        }) 
    }

    confirmDelete = (id) => {
        const title = <h1 className="font-bold">Eliminar Opcion</h1>
        confirmAlert({
          title: title,
          message: '¿Estas seguro de eliminar esta opcion?',
          buttons: [
            {
              label: 'Si',
              onClick: () => this.delete(id)
            },
            {
              label: 'No',
              onClick: () => toast.warn('Operacion cancelada')
            }
          ]
        });
    };
    
    noOptions = () => (
        <div className="flex bg-white font-bold text-lg justify-center w-full">
            <h1>No hay opciones disponibles.</h1>
        </div>
    )
    renderShowOptions = (option, index) => {
        return (
            <div key={index} className="sm:w-full flex justify-center w-full px-64 p-2">
                <div className="bg-white w-full shadow-lg rounded-lg p-6">
                        <div className="sm:w-full md:w-1/3">
                            <img className="h-16 w-16" src={option.icono}/> 
                        </div>  
                        <div className="w-full py-2">
                            <h1 className="text-lg text-left text-blue-600">{option.nombre}</h1>
                            <h1 className="text-lg text-left text-gray-500">precio: ${option.precio}</h1>
                        </div>   
                        <div className="w-full text-gray-500 py-2">
                            <h1 className="font-bold text-base">Descripcion</h1>
                            <span className="text-xs">
                            {option.descripcion} 
                            </span>
                        </div>
                        <div className="w-full">
                            <button className="inline-block bg-yellow-500 py-1 px-3 rounded text-white">editar</button>
                            <button onClick={ ()=> this.confirmDelete(option._id) } className="inline-block m-1 bg-red-500 py-1 px-3 rounded text-white">eliminar</button>
                        </div>
                    </div>
                </div>
        )
        
    }
    async reloadOptions(){
        const {data} = await axios.get(global.BASE_URL+`opciones`)
        this.setState({
            options : data.opciones
        })
    }
    async delete (id){
        const { data } = await axios.delete(global.BASE_URL+`opciones/${id}`,{
            headers:{
                'access-token' : auth.getToken()
            }
        })

        if(data.status){
            toast.success('Opcion eliminada exitosamente')
            this.reloadOptions()
        }
        else{
            localStorage.removeItem('session')
            window.location.href='/login'
        }
    }
    render(){
        return (
            <div className="w-full h-full pb-4">
                <ToastContainer />
                <div className="py-4">
                    <h1 className="text-lg text-gray-600">Tus opciones</h1>
                </div>
                <div className="py-1">
                    <button onClick={() => this.props.changeSectionOptions(this.props.add)} className="inline py-2 rounded text-white px-3 bg-blue-500">agregar opcion</button>
                    <button className="inline m-2 py-2 rounded text-white px-3 bg-blue-500">agregar opcion a servicio</button>
                </div>
                <div className="py-4">
                    <div className="flex flex-wrap bg-gray-200">
                        
                        {
                            this.state.options.length > 0 ? 
                            this.state.options.map((option,index)=>{
                                return this.renderShowOptions(option, index)
                            }) : this.noOptions()
                        }
                    </div>
                </div>
            </div>
        )
    }
}

export default ShowOptions