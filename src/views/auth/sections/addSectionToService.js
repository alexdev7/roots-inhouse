import React, {Component} from 'react'
import Icon from '@material-ui/core/Icon'
import axios from 'axios'
import global from 'api/global'
import auth from 'assets/auth'
import { ToastContainer, toast } from 'react-toastify'
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css';
import Navbar from 'components/Breadcrumb'
class addSectionToService extends Component{
    state={
        //servicios
        services : [],
        //secciones de servicios
        service_sections:[],
        //secciones
        sections : [],
        //resto de secciones que el servicio ya tiene agregadas
        rest : [],
        //id del servicio seleccionado
        service_selected:'',
        //id de la seccion seleccionada
        section_selected:'',
        //propiedades del servicio
        name:'',
        step:'',
        status:'',
        id:'', 
        //index del servicio
        index_service:'',
        //index de la seccion
        index_section:'',

        section:''
    }
    //recargar servicios
    async reloadServices(){
        const { data } = await axios.get(global.BASE_URL+'servicios-admin')
        
        this.setState({
            services : data.servicios 
        })
    }
    //obtener servicios cuando el componente termina de cargar
    async componentDidMount () {
        const { data } = await axios.get(global.BASE_URL+'servicios-admin')
        
        this.setState({
            services : data.servicios 
        })
    }
    
    //confirmar para borrar una seccion
    confirmDelete = (id) => {
        confirmAlert({
          title: <h1 className="font-bold">Eliminar seccion</h1>,
          message: '¿Estas seguro de eliminar esta seccion de este servicio?',
          buttons: [
            {
              label: 'Si',
              onClick: () => this.delete(id)
            },
            {
              label: 'No',
              onClick: () => toast.warn('Operacion cancelada')
            }
          ]
        });
    };

    //obtener secciones del servicio seleccionado y sacar las secciones que faltan por agregar
    async getSections(index){
        if(index >= 0){
            
            //guardar todas las secciones
            const sections = []
            //para almacenar las secciones del servicio que tiene
            const service_sections = []
            //para sacar el resto de secciones faltantes
            const rest = []
            //peticion a secciones
            const { data } = await axios.get(global.BASE_URL+'secciones')
            //mapear las secciones y guardarlas en sections
            data.secciones.map(section=>{
                sections.push(section)
            })
            //obtener las secciones del servicio seleccionado
            this.state.services[index].secciones.map(section=>{
                //almacenar las secciones en service_sections
                service_sections.push(section)
            })
            //obtener el resultado de los faltantes
            const results = sections.filter( ({ _id: id1 }) => !service_sections.some(({ _id: id2 }) => id2 === id1));
            //mapear los objetos de results para obtener los restantes
            results.map(result=>{
                rest.push(result)
            })

            this.setState({
                //asigar el resto de secciones
                rest : rest,
                //asignarle secciones a un servicio
                service_sections: service_sections,
                //propiedades del servicio
                name:this.state.services[index].nombre,
                step:this.state.services[index].step,
                status:this.state.services[index].status,
                id:this.state.services[index]._id,
                //index del servicio seleccionado
                index_service : index,
                //servicio seleccionado 
                service_selected : this.state.services[index]._id
            })

            this.refs.combosections.value = JSON.stringify({ id: 0, index :0})
        }
        else{
            this.setState({
                //asigar el resto de secciones
                rest : '',
                //asignarle secciones a un servicio
                service_sections: '',
                //propiedades del servicio
                name:'',
                step:'',
                status:'',
                id:'',
                //servicio seleccionado
                index_service:'',
                service_selected : ''
            })
        }
    }
    
    render(){
        return(
            <div className="w-full h-full bg-white px-3 py-2 rounded shadow-md">
                <Navbar child="Agregar una seccion a un servicio">
                     <li><a onClick={()=>this.props.changeSectionOfSections(this.props.show)} className="text-blue underline cursor-pointer font-bold">Secciones</a></li>
                </Navbar>
                <ToastContainer />
                
                <div className="py-2 flex flex-wrap -mx-3 mb-2">
                    <div className="w-1/3 px-3 mb-6 md:mb-0">
                        <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="grid-state">
                            Servicios
                        </label>
                        <div className="relative">
                            <select ref="comboservices" onChange={ (event)=> this.getSections(event.target.value) } className="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-state">
                            <option value={ -1 } >Seleccione un servicio</option>
                            {
                                this.state.services ? 
                                this.state.services.length > 0 ?
                                    this.state.services.map((service , index) => <option key={service._id} value={index} >{service.nombre}</option>)
                                : '' : ''
                            }
                            </select>
                            <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                            <svg className="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
                            </div>
                        </div>
                    </div>
                    <div className="w-1/3 px-3 mb-6 md:mb-0">
                        <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="grid-state">
                            Secciones
                        </label>
                        <div className="relative">
                            <select ref="combosections" onChange={(event)=>this.handleSelectedSection(event.target.value)} className="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-state">
                            <option value={JSON.stringify({
                                        id: 0,
                                        index :0, 
                            })}>Seleccione una seccion</option>
                            { /* el resto de secciones de los que ya tiene servicios */ }
                            {
                                this.state.rest ? 
                                    this.state.rest.map((section , index) => <option key={index} value={ JSON.stringify({
                                        id: section._id,
                                        index : index,
                                        obj:section
                                    })} >{section.nombre}</option>)
                                : ''
                            }
                            </select>
                            <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                            <svg className="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
                            </div>
                        </div>
                    </div>
                    <div className="w-full px-3 py-2">
                        <button onClick={()=>this.add()} className="px-3 py-2 bg-blue-500 shadow-lg rounded text-white">agregar</button>
                    </div>
                </div>
                <div className="table w-1/2 pb-10">
                    <div className="table-row-group border border-2 border-gray-500 rounded shadow-lg">
                        {
                            this.state.service_sections ? 
                            this.state.service_sections.map((sections, index)=> {
                                return (
                                    <div key={index} className="table-row">
                                        <div className="table-cell text-gray-700 px-4 py-2 text-sm">{sections.nombre}</div>
                                        <div className="table-cell text-gray-700 px-4 py-2 text-sm">
                                            <button onClick={()=>this.delete(index, sections)} className="inline-block m-1 bg-red-500 py-2 px-3 rounded text-white">eliminar</button>
                                        </div>
                                        
                                    </div>
                                )
                            }) : ( <h1>No hay seleccion de un servicio</h1>)
                        }
                    </div>
                </div>
            </div>
        )
    }

    
    handleSelectedSection(value){
        
        const { id, index, obj } = JSON.parse(value)
        
        if(id){
            this.setState({
                section_selected : id,
                index_section : index,
                section:obj
            })
        }
        else{
            
            this.setState({
                section_selected : '',
                index_section : '',
                obj:''
            })
        }
        
    }

    async add(){
        const acumulate = []
        const service_sections = []
        const rest = []
        
        const { service_selected, section_selected } = this.state
        if(service_selected && section_selected){
            
            const {data} = await axios.get(global.BASE_URL+`servicios/${service_selected}`)
            //recolectar todas las secciones que tenia
            data.servicio.secciones.map(section=>{
                service_sections.push(section)
            })

            service_sections.push(section_selected)
            
            delete this.state.rest[this.state.index_section]
            
            this.state.rest.map(section=>{
                rest.push(section)
            })

            this.setState({
                rest : rest, 
            })

            this.state.service_sections.map(section=>{
                acumulate.push(section)
            })

            acumulate.push(this.state.section)

            this.setState({
                service_sections : acumulate
            })
            this.refs.combosections.value = JSON.stringify({ id: 0, index :0})

            const res = await axios.put(global.BASE_URL+'servicios',{
                id: service_selected,
                status:this.state.status,
                nombre: this.state.name,
                step:this.state.step,
                secciones:service_sections
            },{
                headers:{
                    'access-token' : auth.getToken()
                }
            })

            if(res.data.status){
                toast.success('Seccion agregada correctamente')
            }
            else{
                localStorage.removeItem('session')
                window.location.href='/login'
            }
            
        }
        else{

        }
    }
    async delete(index, section){
        
        const sections = []
        const rest = []
        
        delete this.state.service_sections[index]
        
        this.state.rest.map(sections_rest=>{
            rest.push(sections_rest)
        })

        rest.push(section)
        
        this.state.service_sections.map(section=>{
            sections.push(section._id)
        })
        
        this.setState({
            rest : rest
        })

        const res = await axios.put(global.BASE_URL+'servicios',{
            id: this.state.id,
            status:this.state.status,
            nombre: this.state.name,
            step:this.state.step,
            secciones:sections
        },{
            headers:{
                'access-token' : auth.getToken()
            }
        })

        if(res.data.status){
            
            toast.success('Seccion eliminada correctamente')
            this.reloadServices()
        }
        else{
            localStorage.removeItem('session')
            window.location.href='/login'
        }
        

    }
}

export default addSectionToService