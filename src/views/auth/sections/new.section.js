import React, {Component} from 'react'
import validate from 'validation/validator'
import sectionMessages from 'messages/section'
import Icon from '@material-ui/core/Icon'
import axios from 'axios'
import global from 'api/global'
import auth from 'assets/auth'
import Notifications, {notify} from 'react-notify-toast';
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css';
import Navbar from 'components/Breadcrumb'
const defaultState = {
    sectionName:'',
    stepNumber:'',
    optionselected:'',
    errors:{
        section : false,
        sectionMessage:'',
        step :false,
        stepMessage:'',
        option : false,
        optionMessage:''
    }
}
class NewSection extends Component{
    state={
        sectionName:'',
        stepNumber:'',
        errors:{
            section : false,
            sectionMessage:'',
            step :false,
            stepMessage:''
        }
    }
    render(){
        const { section, sectionMessage, step, stepMessage } = this.state.errors
        return(
            <div className="w-full bg-white px-3 py-2 rounded shadow-md">
                <ToastContainer />
                <Navbar child="Agregar una nueva seccion">
                     <li><a onClick={()=>this.props.changeSectionOfSections(this.props.show)} className="text-blue underline cursor-pointer font-bold">Secciones</a></li>
                </Navbar>
                
                <div className="py-2 flex flex-wrap -mx-3 mb-6">
                    <div className="w-full md:w-full px-3 mb-6 md:mb-0">
                        <label className="block tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="section-name">
                            Nombre de la seccion
                        </label>
                        <input  className="appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" 
                                id="section-name" 
                                ref="section-name"
                                placeholder="Seccion"
                                onChange={ (event)=> this.handleSectionName(event.target.value) }
                        />
                        <p className="text-red-600 font-bold text-sm"> { !section ? sectionMessage : ''  } </p>
                    </div>
                    <div className="w-full md:w-full px-3 mb-6 md:mb-0">
                        <label className="block tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="section-name">
                            Paso de la seccion
                        </label>
                        <input  className="appearance-none block w-1/4 bg-gray-200 text-gray-700 border rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" 
                                id="section-name" 
                                ref="section-name"
                                placeholder="Paso"
                                type="number"
                                min="0"
                                onChange={(event)=>this.handleSectionStep(event.target.value)}
                        />
                        <p className="text-red-600 font-bold text-sm"> { !step ? stepMessage : ''  } </p>
                    </div>
                    <div className="w-full md:w-1/2 px-3">
                        <button onClick={ ()=>this.save() } className="bg-blue-600 py-2 px-3 rounded text-sm text-white shadow-md focus:outline-none">
                            Guardar
                        </button>
                    </div>
                </div>
            </div>
        )   
    }

    handleSectionName = value => {
        if(!validate.isEmpty(value)){
            this.setState(prevstate =>({
                errors:{
                    ...prevstate.errors,
                    ...prevstate.errors.section,
                    section : false,
                    ...prevstate.errors.sectionMessage,
                    sectionMessage : sectionMessages.required
                }
            }))
        }
       
        else{
            this.setState(prevstate =>({
                sectionName:value,
                errors:{
                    ...prevstate.errors,
                    ...prevstate.errors.section,
                    section : true
                }
            }))
        }
    }
    handleSectionStep = value => {
        if(!validate.isEmpty(value)){
            this.setState(prevstate =>({
                errors:{
                    ...prevstate.errors,
                    ...prevstate.errors.step,
                    step : false,
                    ...prevstate.errors.stepMessage,
                    stepMessage : sectionMessages.required
                }
            }))
        }
        else if(!validate.isValid( value, 'number' )){
            this.setState(prevstate =>({
                errors:{
                    ...prevstate.errors,
                    ...prevstate.errors.step,
                    step : false,
                    ...prevstate.errors.stepMessage,
                    stepMessage : sectionMessages.onlyNumbers
                }
            }))
        }
        else{
            this.setState(prevstate =>({
                stepNumber:value,
                errors:{
                    ...prevstate.errors,
                    ...prevstate.errors.step,
                    step : true
                }
            }))
        }
    }
    async save(){
        const { section ,step } = this.state.errors 

        if(!section && !step){
            toast.error('No puedes agregar una seccion todavia!')
        }
        else{

            const res = await axios.post(global.BASE_URL+'secciones',{
                nombre : this.state.sectionName,
                step: this.state.stepNumber
            },{
                headers:{
                    'access-token' : auth.getToken()
                }
            })

            if(res.data.status){
                toast.success('Seccion agregada correctamente')
                this.props.changeSectionOfSections(this.props.show)
            }
            else{
                toast.error('Su sesion ha caducado')
            }

            
        }
    }
}

export default NewSection