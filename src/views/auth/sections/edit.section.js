import React, {Component} from 'react'
import validate from 'validation/validator'
import sectionMessages from 'messages/section'
import Icon from '@material-ui/core/Icon'
import { changeSectionOfSections } from '../../../redux/actions/app.actions'
import { connect } from 'react-redux'
import axios from 'axios'
import global from 'api/global'
import Notifications, {notify} from 'react-notify-toast';
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css';
import auth from 'assets/auth'
import Navbar from 'components/Breadcrumb'
class EditService extends Component{
    state={
        id:'',
        name:'',
        step:'',
        status:'',
        options:[],
        errors:{
            name:true,
            messagename:'',
            step:true,
            messagestep:'',
            status:true,
            messagestatus:''
        }
    }
    
    async componentDidMount(){
        const { data } = await axios.get(global.BASE_URL+'secciones/'+this.props.page.parameter_section)
        this.setState({
            id: data.seccion._id,
            name : data.seccion.nombre,
            step : data.seccion.step,
            status : data.seccion.status,
            options: data.seccion.opciones
        })
        
    }
    render(){
        const { name, messagename, step, messagestep} = this.state.errors
        return(
            <div className="w-full bg-white px-3 py-2 rounded shadow-md">
                <ToastContainer />
                <Navbar child="Editar una seccion">
                     <li><a onClick={()=>this.props.changeSectionOfSections(this.props.show)} className="text-blue underline cursor-pointer font-bold">Secciones</a></li>
                </Navbar>
                
                <div className="py-2 flex flex-wrap -mx-3 mb-6">
                    <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                        <label className="block tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="section-name">
                            Nombre de la seccion
                        </label>
                        <input onChange={ (event) => this.handleNameSection(event.target.value)} className="appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" 
                                id="section-name" 
                                ref="section-name"
                                placeholder="Seccion"
                                defaultValue={this.state.name}
                        />
                        <p className="text-red-600 text-sm font-bold"> { !name ? messagename : '' } </p>
                    </div>
                    <div className="w-1/3 px-3 mb-6 md:mb-0">
                        <label className="block tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="section-name">
                            Paso de la seccion
                        </label>
                        <input onChange={(event) => this.handleStepSection(event.target.value)}  className="appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" 
                                id="section-name" 
                                ref="section-name"
                                placeholder="Paso"
                                defaultValue={this.state.step}
                        />
                        <p className="text-red-600 text-sm font-bold"> { !step ? messagestep : '' } </p>
                    </div>
                    <div className="w-1/2 px-3 mb-6 md:mb-0">
                        <label className="block tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="section-name">
                            Estado de la seccion
                        </label>
                        <div className="relative">
                            <select value={this.state.status} onChange={(event)=>this.handleStatusSection(event.target.value)} className="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-state">
                            <option value={0}>Seleccione un estado</option>
                            <option value={true}>Activo</option>
                            <option value={false}>Inactivo</option>
                            </select>
                            <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                            <svg className="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
                            </div>
                        </div>
                    </div>
                    <div className="w-full mt-3 px-3">
                        <button onClick={ ()=> this.edit()} className="bg-blue-600 py-2 px-3 rounded text-sm text-white shadow-md">
                            Guardar
                        </button>
                    </div>
                </div>
            </div>
        )   
    }

    handleNameSection = value => {
        if(!validate.isEmpty(value)){
            this.setState(prevstate => ({
                name : '',
                errors:{
                    ...prevstate.errors,
                    ...prevstate.errors.name,
                    name : false,
                    ...prevstate.errors.messagename,
                    messagename : sectionMessages.required
                }
            }))
        }
        else if(!validate.isValid(value, 'string')){
            this.setState(prevstate => ({
                name:'',
                errors:{
                    ...prevstate.errors,
                    ...prevstate.errors.name,
                    name : false,
                    ...prevstate.errors.messagename,
                    messagename : sectionMessages.onlyLetters
                }
            }))
        }
        else{
            this.setState(prevstate=>({
                name : value,
                errors:{
                    ...prevstate.errors,
                    ...prevstate.errors.name,
                    name : true,
                    ...prevstate.errors.messagename,
                    messagename : ''
                }
            }))
        }
    }
    handleStepSection = value => {
        
        if(!validate.isEmpty(value)){
            this.setState(prevstate => ({
                step : '',
                errors:{
                    ...prevstate.errors,
                    ...prevstate.errors.step,
                    step : false,
                    ...prevstate.errors.messagestep,
                    messagestep : sectionMessages.required
                }
            }))
        }
        else if(!validate.isValid(value, 'number')){
            this.setState(prevstate => ({
                step : '',
                errors:{
                    ...prevstate.errors,
                    ...prevstate.errors.step,
                    step : false,
                    ...prevstate.errors.messagestep,
                    messagestep : sectionMessages.onlyNumbers
                }
            }))
        }
        else{
            this.setState(prevstate => ({
                step : value,
                errors:{
                    ...prevstate.errors,
                    ...prevstate.errors.step,
                    step : true,
                    ...prevstate.errors.messagestep,
                    messagestep : ''
                }
            }))
        }
    }
    handleStatusSection = value => {
        if(value != "0"){
            this.setState(prevstate=>({
                status : value,
                errors:{
                    ...prevstate.errors,
                    ...prevstate.errors.status,
                    status : true
                }
            }))
        }
        else{
            this.setState(prevstate=>({
                status : '',
                errors:{
                    ...prevstate.errors,
                    ...prevstate.errors.status,
                    status : false
                }
                
            }))
        }
    }

    async edit(){
        const { name, status, step } = this.state.errors
        if(!name || !step){
            toast.error('Verifique datos de nombre y paso de la seccion')
        }
        else if(!status){
            toast.error('Seleccione un estado para la seccion')
        }
        else{
            const {data} = await axios.put(global.BASE_URL+'secciones',{
                id : this.state.id,
                nombre : this.state.name, 
                step: this.state.step,
                status : this.state.status,
                opciones: this.state.options
            },{
                headers:{
                    'access-token' : auth.getToken()
                }
            })

            if(data.status){
                toast.success('seccion actualizada correctamente')
                this.props.changeSectionOfSections(this.props.show)
            }
            else{
                localStorage.removeItem('session')
                window.location.href='/login'
            }
        }
    }
}

const mapStateToProps = (state) => {
    return {
        page : state.page
    }
}

const dispatchStateToProps = {
    changeSectionOfSections
}
export default connect(mapStateToProps,dispatchStateToProps)(EditService)