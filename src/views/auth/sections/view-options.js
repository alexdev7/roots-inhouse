import React, {Component} from 'react'
import {toast, ToastContainer} from 'react-toastify'
import Icon from '@material-ui/core/Icon'
import {connect} from 'react-redux'

import axios from 'axios'
import global from 'api/global'

import auth from 'assets/auth'

import Navbar from 'components/Breadcrumb'
class ViewOptionsSections extends Component {
    state={
        options : []
    }
    async componentDidMount(){
        const { data } = await axios.get(global.BASE_URL+`secciones/${this.props.page.parameter_section.id}`)
        this.setState({options : data.seccion.opciones})
    }
    async delete(id, index){
        const { data } = await axios.delete(global.BASE_URL+`opciones/${id}`,{
            headers:{
                'access-token' : auth.getToken()
            }
        })

        if(data.status){
            var options = [...this.state.options]
            delete options[index]
            this.setState({
                options : options
            })
        }
        else{
            console.log(data)
        }
       
    }
    render(){
        return(
            <div className="w-full h-full pb-5 bg-white px-3 py-2 rounded shadow-md">
                <ToastContainer />
                <Navbar child="Opciones">
                    <li><a onClick={()=>this.props.changeSectionOfSections(this.props.show)} className="text-blue underline cursor-pointer font-bold">Secciones</a></li>
                    <li>
                        <span className="mx-2">/</span>
                    </li>
                    <li><a className="text-blue cursor-pointer font-bold">{this.props.page.parameter_section.name}</a></li>
                </Navbar>
                <div className="py-3">
                    <button onClick={()=> this.props.changeSectionOfSections(this.props.add, {
                        id:this.props.page.parameter_section.id,
                        name : this.props.page.parameter_section.name
                    }) } className="my-2 bg-blue-500 px-3 rounded py-2 text-white">agregar una opción</button>
                </div>
                <div className="w-full h-full">
                    <div className="flex flex-wrap">
                        {
                            this.state.options.map((option, index)=>{
                                return (
                                <div key={index} className="w-1/3 p-2">
                                    <div className="text-gray-700 border rounded-lg shadow-lg border-blue-500 p-2">
                                        <div className="flex justify-end">
                                            <button onClick={()=> this.props.changeSectionOfSections(this.props.edit,{
                                                id:this.props.page.parameter_section,
                                                opcion:option
                                            })} className="inline flex justify-center py-1 rounded border border-yellow-500 px-1"> <Icon>edit</Icon> </button>
                                            <button onClick={()=>this.delete(option._id, index)} className="inline flex justify-center py-1 rounded border border-red-500 ml-2 px-1"> <Icon>close</Icon> </button>
                                        </div>
                                        <img className="h-24" src={option.icono}/>
                                        <div className="w-full px-2">
                                            <h1 className="font-bold"> Nombre:<span className="font-normal"> {option.nombre}  </span></h1>
                                            <h1>Precio: {option.precio}</h1>
                                            <h1>Tiempo: {option.tiempo}</h1>
                                        </div>
                                    </div>
                                </div>)
                            })
                        }
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        page : state.page
    }
}

export default connect(mapStateToProps)(ViewOptionsSections)