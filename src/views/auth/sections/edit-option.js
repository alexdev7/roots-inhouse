import React, {Component} from 'react'
import Icon from '@material-ui/core/Icon'
import axios from 'axios'
import global from 'api/global'
import auth from 'assets/auth'

import {connect} from 'react-redux'

import { ToastContainer, toast } from 'react-toastify'

import Navbar from 'components/Breadcrumb'
import Separator from 'components/separator'

class EditOption extends Component{
    state={
        name:'',
        descripcion:'',
        file:null,
        id:'',
        selectedFile:null,
        price:'',
        time:'',
        sections:[],
        selectedSection:''
    }
    async componentDidMount(){
        this.setState({
            id:this.props.page.parameter_section.opcion._id,
            name : this.props.page.parameter_section.opcion.nombre,
            descripcion: this.props.page.parameter_section.opcion.descripcion,
            price:this.props.page.parameter_section.opcion.precio,
            time:this.props.page.parameter_section.opcion.tiempo,
            selectedFile:this.props.page.parameter_section.opcion.icono
        })
    }
    render(){
        
        return (
            <div className="w-full h-full bg-white px-3 py-2 rounded shadow-md">
                <ToastContainer />
                <Navbar child="Editar opción">
                    <li><a onClick={()=>this.props.changeSectionOfSections(this.props.show)} className="text-blue underline cursor-pointer font-bold">Secciones</a></li>
                    <Separator/>
                    <li><a onClick={()=>this.props.changeSectionOfSections(this.props.show_options, this.props.page.parameter_section.id)} className="text-blue underline cursor-pointer font-bold">Opciones</a></li>
                    <Separator/>
                    <li><a className="text-blue font-bold">{this.props.page.parameter_section.opcion.nombre}</a></li>
                </Navbar>
                
                <div className="py-2 w-full">
                    <div className="h-32 w-40 px-3 flex items-center justify-center bg-gray-300">
                        <input onChange={(event)=>this.handleSelectImage(event.target.files[0])} type="file" id="files" className="hidden"/>
                        <img className="h-32 w-64" src={this.props.page.parameter_section.opcion.icono}/>
                    </div>
                    {
                        this.props.page.parameter_section.opcion.icono ?
                        <div className="py-4">
                            <input onChange={(event)=>this.handleSelectImage(event.target.files[0])} type="file" id="change_file" className="hidden"/>
                            <label htmlFor="change_file" className="py-2 px-3 text-white rounded bg-blue-500">cambiar</label>
                        </div> : 
                        ''
                    }
                    <div className="py-2 w-1/2 flex flex-wrap -mx-3 mb-6">
                        <div className="w-1/2 px-3 mb-6 md:mb-0">
                            <label className="block tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="name">
                                Opcion
                            </label>
                            <input defaultValue={this.props.page.parameter_section.opcion.nombre} onChange={ (event)=> this.handleNameOption(event.target.value) } className="appearance-none block w-full bg-gray-200 text-gray-700 rounded py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:border-gray-500" placeholder="escribe una opcion"/>
                            {
                                this.state.name ? '' : <h1 className="text-red-600 font-bold">Este campo es requerido</h1> 
                            }
                        </div>
                        <div className="w-1/2 px-3 mb-6 md:mb-0">
                            <label className="block tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="name">
                                Precio de la opcion
                            </label>
                            <input defaultValue={this.props.page.parameter_section.opcion.precio} onChange={ (event) => this.handlePriceOption(event.target.value) } className="appearance-none block w-full bg-gray-200 text-gray-700 rounded py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:border-gray-500" placeholder="define un precio"/>
                            { this.state.price ? '' : <h1 className="text-red-600 font-bold">Este campo es requerido</h1> }
                        </div>
                        <div className="w-1/2 px-3 mb-6 md:mb-0">
                            <label className="block tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="name">
                                Tiempo de realización
                            </label>
                            <input defaultValue={this.props.page.parameter_section.opcion.tiempo} onChange={ (event) => this.handleTime(event.target.value) } className="appearance-none block w-full bg-gray-200 text-gray-700 rounded py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:border-gray-500" placeholder="define un tiempo"/>
                            { this.state.time ? '' : <h1 className="text-red-600 font-bold">Este campo es requerido</h1> }
                        </div>
                        <div className="w-full px-3 mb-6 md:mb-0">
                            <label className="block tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="name">
                                Descripcion
                            </label>
                            <textarea defaultValue={this.props.page.parameter_section.opcion.descripcion} onChange={ (event)=>this.handleDescriptionOption(event.target.value) } className="appearance-none w-full block bg-gray-200 text-gray-700 rounded py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:border-gray-500" placeholder="Escribe una descripcion"></textarea>
                            { this.state.descripcion ? '' : <h1 className="text-red-600 font-bold">Este campo es requerido</h1> } 
                        </div>
                        
                        <div className="w-full py-3 px-3">
                            <button onClick={()=> this.addOption()} className="py-2 px-3 bg-blue-500 rounded text-xs text-white">Agregar</button>
                        </div>
                    </div>
                    
                </div>
            </div>
        )
    }

    handleSelectImage(value){
        this.setState({
            file: URL.createObjectURL(value),
            selectedFile : value
        })
    }

    handleNameOption = (value) => {
        this.setState({
            name : value
        })
    }
    handlePriceOption = (value) => {
        this.setState({
            price : value
        })

    }
    handleDescriptionOption = (value) => {
        this.setState({
            descripcion : value
        })
    }
    handleSelectedSection = (value) => {
        if(value != 0){
            this.setState({
                selectedSection : value
            })
        }
    }
    handleTime = (value) => {
        this.setState({
            time : value
        })
    }

    async addOption(){
        const { name, price, descripcion, time } = this.state
        
        if(!name && !price && !descripcion && !time){
            toast.error('Verifique sus campos')
        }
        
        else{
            const { name, descripcion, price, time } = this.state
            console.log({name, descripcion, price, time})
            
            const form = new FormData();
            form.append('id',this.state.id)
            form.append('icono', this.state.selectedFile)
            form.append("nombre", this.state.name);
            form.append("precio", this.state.price);
            form.append("descripcion", this.state.descripcion);
            form.append("tiempo",this.state.time)

            
            //add option
            const editOption = await axios.put(global.BASE_URL+'opciones',
            form,
            { headers:{'access-token' : auth.getToken() }})
           
            if(editOption.data.status){
                
                this.props.changeSectionOfSections(this.props.show_options, this.props.page.parameter_section.id)
            }
            else{
                console.log(editOption.data)
            }
            
            
        }        
    }
}

const mapStateToProps = (state) => {
    return {
        page : state.page
    }
}
export default connect(mapStateToProps)(EditOption)