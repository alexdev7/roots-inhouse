import React, {Component} from 'react'
import Icon from '@material-ui/core/Icon'
import axios from 'axios'
import global from 'api/global'
import auth from 'assets/auth'

import {connect} from 'react-redux'

import { ToastContainer, toast } from 'react-toastify'

import Navbar from 'components/Breadcrumb'
class NewOption extends Component{
    state={
        name:'',
        descripcion:'',
        file:null,
        selectedFile:null,
        price:'',
        time:'',
        sections:[],
        selectedSection:''
    }
    async componentDidMount(){
        const {data} = await axios.get(global.BASE_URL+'secciones')
        this.setState({
            sections : data.secciones
        })
    }
    render(){
        
        return (
            <div className="w-full h-full bg-white px-3 py-2 rounded shadow-md">
                <ToastContainer />
                <Navbar child="Agregar opcion">
                    <li><a onClick={()=>this.props.changeSectionOfSections(this.props.show)} className="text-blue underline cursor-pointer font-bold">Secciones</a></li>
                    <li>
                        <span className="mx-2">/</span>
                    </li>
                    <li><a onClick={()=>this.props.changeSectionOfSections(this.props.show_options, this.props.page.parameter_section)} className="text-blue cursor-pointer font-bold underline">{this.props.page.parameter_section.name}</a></li>
                </Navbar>
                
                <div className="py-2 w-full">
                    <div className="h-32 w-40 px-3 flex items-center justify-center bg-gray-300">
                        <input onChange={(event)=>this.handleSelectImage(event.target.files[0])} type="file" id="files" className="hidden"/>
                        {
                            this.state.file ? 
                                <img className="h-32 w-64" src={this.state.file}/>
                            : <label htmlFor="files">Selecciona una imagen</label>
                        }
                    </div>
                    {
                        this.state.file ?
                        <div className="py-4">
                            <input onChange={(event)=>this.handleSelectImage(event.target.files[0])} type="file" id="change_file" className="hidden"/>
                            <label htmlFor="change_file" className="py-2 px-3 text-white rounded bg-blue-500">cambiar</label>
                        </div> : 
                        ''
                    }
                    <div className="py-2 w-1/2 flex flex-wrap -mx-3 mb-6">
                        <div className="w-1/2 px-3 mb-6 md:mb-0">
                            <label className="block tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="name">
                                Opcion
                            </label>
                            <input onChange={ (event)=> this.handleNameOption(event.target.value) } className="appearance-none block w-full bg-gray-200 text-gray-700 rounded py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:border-gray-500" placeholder="escribe una opcion"/>
                            {
                                this.state.name ? '' : <h1 className="text-red-600 font-bold">Este campo es requerido</h1> 
                            }
                        </div>
                        <div className="w-1/2 px-3 mb-6 md:mb-0">
                            <label className="block tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="name">
                                Precio de la opcion
                            </label>
                            <input onChange={ (event) => this.handlePriceOption(event.target.value) } className="appearance-none block w-full bg-gray-200 text-gray-700 rounded py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:border-gray-500" placeholder="define un precio"/>
                            { this.state.price ? '' : <h1 className="text-red-600 font-bold">Este campo es requerido</h1> }
                        </div>
                        <div className="w-1/2 px-3 mb-6 md:mb-0">
                            <label className="block tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="name">
                                Tiempo de realización
                            </label>
                            <input onChange={ (event) => this.handleTime(event.target.value) } className="appearance-none block w-full bg-gray-200 text-gray-700 rounded py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:border-gray-500" placeholder="define un precio"/>
                            { this.state.time ? '' : <h1 className="text-red-600 font-bold">Este campo es requerido</h1> }
                        </div>
                        <div className="w-full px-3 mb-6 md:mb-0">
                            <label className="block tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="name">
                                Descripcion
                            </label>
                            <textarea onChange={ (event)=>this.handleDescriptionOption(event.target.value) } className="appearance-none w-full block bg-gray-200 text-gray-700 rounded py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:border-gray-500" placeholder="Escribe una descripcion"></textarea>
                            { this.state.descripcion ? '' : <h1 className="text-red-600 font-bold">Este campo es requerido</h1> } 
                        </div>
                        
                        <div className="w-full py-3 px-3">
                            <button onClick={()=> this.addOption()} className="py-2 px-3 bg-blue-500 rounded text-xs text-white">Agregar</button>
                        </div>
                    </div>
                    
                </div>
            </div>
        )
    }

    handleSelectImage(value){
        this.setState({
            file: URL.createObjectURL(value),
            selectedFile : value
        })
    }

    handleNameOption = (value) => {
        this.setState({
            name : value
        })
    }
    handlePriceOption = (value) => {
        this.setState({
            price : value
        })

    }
    handleDescriptionOption = (value) => {
        this.setState({
            descripcion : value
        })
    }
    handleSelectedSection = (value) => {
        if(value != 0){
            this.setState({
                selectedSection : value
            })
        }
    }
    handleTime = (value) => {
        if(value != 0){
            this.setState({
                time : value
            })
        }
    }

    async addOption(){
        const { name, price, descripcion, time } = this.state
        if(!name && !price && !descripcion && !time){
            toast.error('Verifique sus campos')
        }
        
        else{
            var options = []
            
            const getSection = await axios.get(global.BASE_URL+`secciones/${this.props.page.parameter_section.id}`)
            
            getSection.data.seccion.opciones.map(option=>{
                options.push(option._id)
            })

            const form = new FormData();
            form.append('icono', this.state.selectedFile)
            form.append("nombre", this.state.name);
            form.append("precio", this.state.price);
            form.append("descripcion", this.state.descripcion);
            form.append("tiempo",this.state.time)

            //add option
            const addOption = await axios.post(global.BASE_URL+'opciones',
            form,
            { headers:{'access-token' : auth.getToken() }})
           
            if(addOption.data.status){
                options.push(addOption.data.opcion._id)

                const updateSection = await axios.put(global.BASE_URL+`secciones`,{
                    id: this.props.page.parameter_section.id,
                    nombre: getSection.data.seccion.nombre,
                    step: getSection.data.seccion.step,
                    status : getSection.data.seccion.status,
                    opciones : options
                },{
                    headers:{
                        'access-token' : auth.getToken()
                    }
                })

                if(updateSection.data.status){
                    this.props.changeSectionOfSections(this.props.show_options, this.props.page.parameter_section)
                }
                else{
                    toast.error(addOption.data)
                }
            }
            else{
                console.log(addOption.data)
            }
            
        }        
    }
}

const mapStateToProps = (state) => {
    return {
        page : state.page
    }
}
export default connect(mapStateToProps)(NewOption)