import React, {Component} from 'react'
import axios from 'axios'
import global from 'api/global'
import Icon from '@material-ui/core/Icon'
import auth from 'assets/auth'
import { ToastContainer, toast } from 'react-toastify'
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css';
import Navbar from 'components/Breadcrumb'
class ShowSections extends Component{

    state={
        sections : [],
        search : '',
        loading : true
    }

    confirmDelete = (id) => {
        confirmAlert({
          title: <h1 className="font-bold">Eliminar seccion</h1>,
          message: '¿Estas seguro de eliminar esta seccion?',
          buttons: [
            {
              label: 'Si',
              onClick: () => this.delete(id)
            },
            {
              label: 'No',
              onClick: () => toast.warn('Operacion cancelada')
            }
          ]
        });
    };

    async reload(){
        const { data } = await axios.get(global.BASE_URL+'secciones-admin')
        
        this.setState({
            sections : data.secciones,
            loading : false
        })
    }
    async componentDidMount () {
        const { data } = await axios.get(global.BASE_URL+'secciones-admin')
        
        this.setState({
            sections : data.secciones,
            loading : false
        })
    }

    isLoadingSections = () => <h1>Cargando secciones...</h1>
    noSections = () =>  <h1>No se han encontrados secciones registrados</h1>


    renderSections = (section, index) => {
        
        if(section){
            return(
                <div key={index} className="table-row">
                    <div className="table-cell text-gray-700 px-4 py-2 text-sm">
                        {section.nombre}
                    </div>
                    <div className="table-cell text-gray-700 px-4 py-2 text-sm">
                        {section.step}
                    </div>
                    <div className="table-cell text-gray-700 px-4 py-2 text-sm">
                        {section.status ? 'Activo' : 'Inactivo'}
                    </div>
                    <div className="table-cell flex p-2 flex-wrap text-gray-700 px-4 py-2 text-sm">
                        <button onClick={ ()=> this.props.changeSectionOfSections(this.props.edit, section._id) } className="inline-block m-1 bg-yellow-500 px-3 py-2 text-sm rounded text-white"> editar </button>
                        <button onClick={ ()=> this.confirmDelete(section._id) } className="inline-block m-1 bg-red-500 px-3 py-2 text-sm rounded text-white"> eliminar </button>
                        <button onClick={ ()=> this.props.changeSectionOfSections(this.props.options, {
                            id:section._id,
                            name : section.nombre
                        }) } className="inline-block m-1 bg-blue-500 px-3 py-2 text-sm rounded text-white"> agregar opciones </button>
                    </div>
                </div>
            )
        }

        return this.noSections()
    }

    noSectionsFounded = () => <h1>No hay secciones agregadas</h1>
    
    render(){
        const {search} = this.state

        const filteredSections = this.state.sections.filter( section => {
            return section.nombre.toLowerCase().indexOf(search.toLowerCase()) !== -1 || section.step.toLowerCase().indexOf(search.toLowerCase()) !== -1 || section.nombre.toUpperCase().indexOf(search.toUpperCase()) !== -1
        })

        if(this.state.loading){
            return this.isLoadingSections()
        }
        else{
            return (
                <div className="w-full overflow-auto pb-10 px-3 bg-white py-2 rounded shadow-md">
                    <ToastContainer/>
                    <Navbar>
                     <li><a className="text-blue cursor-pointer font-bold">Secciones</a></li>
                    </Navbar>
                    <div className="py-3 w-full">
                        <button onClick={ ()=> this.props.changeSectionOfSections(this.props.add) } className="inline-block text-white m-1 bg-blue-500 px-3 py-2 text-sm rounded"> agregar seccion </button>
                    </div>
                    <div>
                        <a className="cursor-pointer underline" onClick={ ()=>this.props.changeSectionOfSections(this.props.services) }>agrega una seccion a un servicio</a>
                    </div>
                    <div className="py-2">
                        <div className="w-1/2 py-2">  
                            <div className="relative">
                                <input autoComplete="off" onChange={ (event)=> this.handleSearch(event) } className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-10 text-gray-700 leading-tight focus:outline-none" placeholder="busca algo ..." id="inline-full-name" type="text" /> 
                                <div className="pointer-events-none absolute inset-y-0 left-0 flex items-center px-2 text-gray-700">
                                    <span className="material-icons text-gray-600">
                                    search
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div className="table w-full">
                            <div className="table-row-group">
                                <div className="table-row">
                                    <div className="table-cell text-gray-700 px-4 py-2 text-sm">Nombre</div>
                                    <div className="table-cell text-gray-700 px-4 py-2 text-sm">Paso</div>
                                    <div className="table-cell text-gray-700 px-4 py-2 text-sm">Estado</div>
                                    <div className="table-cell text-gray-700 px-4 py-2 text-sm">Acciones</div>
                                </div>
                                {
                                    filteredSections.map((service, index)=>{
                                        return this.renderSections(service, index)
                                    })    
                                }
                            </div>
                    </div>
                    </div>
                </div>
            )
        }
    }

    handleSearch = (e) => {
        this.setState({
            search : e.target.value
        })
    }

    async delete (id){
        try {   
            const {data} = await axios.delete(global.BASE_URL+'secciones/'+id,{
                headers : {
                    'access-token' : auth.getToken()
                }
            })

            if(data.status){
                toast.success('Seccion eliminada correctamente')
                this.reload()
            }
            else{
                localStorage.removeItem('session')
                window.location.href='/login'
                
            }
        
        } catch (error) {
            
        }
    }
}

export default ShowSections