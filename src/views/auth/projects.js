import React, {Component} from 'react'
import {connect} from 'react-redux'

import Show from './projects/show-projects'
import ViewProject from './projects/view-project'

import {changeProjectView} from '../../redux/actions/app.actions'
class Projects extends Component{
    changeProjectView = (index, parameter = null) => {
        this.props.changeProjectView(index, parameter)
    }
    renderView(){
        switch(this.props.page.project_view){
            case 0:
                return <Show changeProjectView={this.changeProjectView}  view={1}/>
            case 1:
                return <ViewProject changeProjectView={this.changeProjectView}  show={0}/>
            default:
                return <Show changeProjectView={this.changeProjectView} view={1}/>

        }
    }
    render()
    {
        return(
            <div className="p-3">
                { this.renderView() }   
            </div>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        page : state.page
    }
}
const dispatchStateToProps = {
    changeProjectView
}

export default connect(mapStateToProps, dispatchStateToProps)(Projects)