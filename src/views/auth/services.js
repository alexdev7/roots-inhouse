import React , { Component } from 'react'

import { changeServicesSections } from '../../redux/actions/app.actions'

import { connect } from 'react-redux'

import Show from './services/new.service'
import Edit from './services/edit.service'


class Services extends Component{
    constructor(props){
        super(props)
        
    }
    changeServicesSections = (number, parameter = null) => {
        this.props.changeServicesSections(number, parameter)
    }
    renderView(){
        switch(this.props.page.services){
            case 0:
                return <Show changeServicesSections = { this.changeServicesSections } number={ 1 } />
            case 1:
                return <Edit changeServicesSections = { this.changeServicesSections } number={0}/>
            default:
                return <Show changeServicesSections = { this.changeServicesSections } number={ 1 }/>

        }
    }
    render()
    {
        return(
            
        <div className="p-3">
            { this.renderView() }   
        </div>
                  
        )
    }
}

const mapStateToProps = (state) => {
    return {
        page : state.page
    }
}
const dispatchStateToProps = {
    changeServicesSections
}

export default connect(mapStateToProps, dispatchStateToProps)(Services)