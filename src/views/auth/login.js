import React, {Component} from 'react'
import logo from '../../assets/logowhite.png'
import auth from '../../assets/auth'
import { UncontrolledAlert } from 'reactstrap'
import CircularProgress from '@material-ui/core/CircularProgress'

class Login extends Component{
    state = {
        user : '',
        pass : '',
        messages : false,
        errorMessage : '', 
        loading:false
    }
    
    async loginHandle (){

        this.setState({
            loading : true
        })

        auth.user = this.state.user
        auth.pass = this.state.pass

        const res = (await auth.login()).data

        if(!res.status){

            this.setState({
                messages:true,
                errorMessage : res.mensaje,
                loading:false
            })
        }
        else{
            
           if(res.user.rol == 'admin'){

                let session = {
                    id : res.user._id,
                    user : res.user.user,
                    name: res.user.nombre_completo,
                    rol:res.rol,
                    token : res.token
                }

                localStorage.setItem('session', JSON.stringify(session))

                this.props.history.push('/private/home')
           }
           else{
                let session = {
                    id : res.user._id,
                    user : res.user.user,
                    name: res.user.nombre_completo,
                    rol:res.user.rol,
                    token : res.token
                }
                
                localStorage.setItem('session-seller', JSON.stringify(session))

                this.props.history.push('/')
                
           }
            
        }

        
    }

    render(){ 
        if(auth.isAuthenticated()){
            window.location.href='/private/home'
        }  
        return(
            <div className="bg-blue-500 h-screen">
                <div class="w-full max-w-xs mx-auto py-12">
                    <div class="bg-white border border-white pb-8 w-full max-w-sm">
                    <div className="bg-blue-500 px-8 py-8">
                        <img src={logo}/>
                    </div>
                    <div className={`bg-red-500 flex text-white justify-between px-4 py-2 ${ this.state.messages ? '' : 'hidden' }`}>
                        <span className="px-2 py-2"> { this.state.errorMessage } </span>
                        <button onClick={ ()=>{ this.setState({
                            messages : false
                        }) } } className="bg-red-800 px-4 py-2 flex-none rounded-full shadow outline-none focus:outline-none"> X </button>
                    </div>
                    <div class="md:flex md:items-center px-8 py-6 ">
                        <div class="md:w-1/3">
                        <label class="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4" for="inline-full-name">
                            Usuario
                        </label>
                        </div>
                        <div class="md:w-2/3">
                        <input onChange = { event => this.setState({
                                user : event.target.value
                            })} class="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white " id="inline-full-name" type="text"/>
                        </div>
                    </div>
                    <div class="md:flex md:items-center px-6">
                    <div class="md:w-1/3">  
                    <label class="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-10" for="inline-username">
                        Contraseña
                    </label>
                    </div>
                    <div class="md:w-2/3">
                    <input  onChange = { event => this.setState({
                                pass : event.target.value
                            })} class="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white" id="inline-username" type="password" placeholder="******************"/>
                    </div>
                    </div>
                        <div class="md:flex pt-10 md:items-center flex justify-center">
                            <div class="w-full flex justify-center">
                                <button onClick={()=>{
                                    this.loginHandle()
                                }} class="shadow bg-blue-500 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded" type="button">
                                    {
                                        this.state.loading ? 
                                        <CircularProgress size={20} color="inherit"/> : 
                                        'Log In'
                                    }
                                </button>
                            </div>
                        </div>
                    </div>
                </div> 
            </div>           
        )
    }
}

export default Login