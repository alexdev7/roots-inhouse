import React , {Component} from 'react'
import logo from 'assets/Logo-Roots.svg'
import auth from 'assets/auth'

class NavBarSeller extends Component{
    render(){
        return(
        <nav className="flex items-center justify-between flex-wrap px-5 py-2">
            <div className="flex items-center flex-shrink-0 text-white mr-6">
                <img src={logo} className="h-12 md:h-24"/>
            </div>
            <div className="w-full block flex-grow lg:flex lg:items-center lg:w-auto">
              <div className="text-lg lg:flex-grow">
              {
                this.props.isLoggedIn ? 
                <button onClick={()=>{
                    window.location.href="/private/home"
                }} className="inline-block text-sm px-4 py-2 md:ml-2 sm:ml-0 leading-none border rounded text-white border-white mt-4 hover:text-black hover:bg-white lg:mt-0 focus:outline-none">{this.props.dashboard_button}</button> : ''
              }
              </div>
              <div>
                <select onChange={ (event)=>this.props.questionHandler(event.target.value) } className="inline-block text-sm w-56 bg-roots text-white sm:ml-0 md:ml-2 h-8 leading-none border rounded border-white focus:outline-none mt-4 lg:mt-0">
                    {
                        this.props.languageCodes.map(lang=>{
                            return (
                                <option key={lang.language} value={lang.language}>
                                    {lang.name}
                                </option>
                            )
                        })
                    }
                </select>
                {
                    this.props.isLoggedIn ?
                    <button onClick={()=>{
                        this.props.closeSession()
                    }} className="inline-block text-sm px-4 py-2 md:ml-2 sm:ml-0 leading-none border rounded text-white border-white mt-4 hover:text-black hover:bg-white lg:mt-0 focus:outline-none">{this.props.logout_text}</button> : ''
                }
              </div>
              
            </div>
        </nav>
        )
    }
}

export default NavBarSeller