import React , { Component} from 'react'
import axios from 'axios';
import global from 'api/global'
import auth from 'assets/auth'
import {toast, ToastContainer} from 'react-toastify'
import messages from 'messages/seller'
import validator from 'validation/validator'
import Navbar from 'components/Breadcrumb'
class AddSeller extends Component{
    state={
        password:'',
        complete_name:'',
        user:'',
        errors:{
            username:true,
            message_username:'',
        },
    }
    componentDidMount(){
        this.setState({
            password : this.makeid(8)
        })
    }
    makeid = (length) => {
        var result           = '';
        var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for ( var i = 0; i < length; i++ ) {
           result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }
    render(){
        const {username} = this.state.errors
        return(
            <div className="w-full h-full bg-white px-3 py-2 rounded shadow-md">
                <ToastContainer/>
                <Navbar child="Agregar vendedor">
                    <li><a onClick={()=>this.props.changeSellerSection(this.props.show)} className="text-blue underline cursor-pointer font-bold">Vendedores</a></li>
                </Navbar>
                <div className="flex flex-wrap -mx-3 mb-6 mt-5">
                    <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                        <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-first-name">
                            Nombre completo
                        </label>
                        <input onChange={(event)=>this.setState({
                            complete_name : event.target.value
                        })} class="appearance-none block w-full bg-gray-200 text-gray-700  rounded py-3 px-4 mb-3 leading-tight focus:outline-none" id="grid-first-name" type="text" placeholder="Jane"/>
                    </div>
                    <div class="w-full md:w-1/2 px-3 mb-8 md:mb-0">
                        <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-first-name">
                            Usuario (correo)
                        </label>
                        <input onChange={(event)=>this.handleUsername(event.target.value)} class="appearance-none block w-full bg-gray-200 text-gray-700 rounded py-3 px-4 mb-3 leading-tight focus:outline-none" id="grid-first-name" type="text" placeholder="Jane"/>
                        { username ? '' : <p>El correo ingresado es invalido</p> }
                    </div>
                    <div class="w-full md:w-1/2 px-3 mb-8 md:mb-0">
                        <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-first-name">
                            Contraseña predeterminada
                        </label>
                        <input ref="password" value={this.state.password} class="appearance-none block w-full bg-gray-200 text-gray-700 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" id="grid-first-name" type="text" placeholder="Jane"/>
                    </div> 
                </div>
                <button onClick={()=>this.handleCreateSelleer()} className="bg-blue-500 py-2 px-3 text-white rounded">agregar</button>
            </div>
        )
    }
    handleUsername = (value) => {
        if(!validator.validateEmail(value)){
            this.setState(prevstate =>({
                errors:{
                    ...prevstate.errors,
                    ...prevstate.errors.username,
                    username : false,
                }
            }))
        }   
        else{
            this.setState(prevstate =>({
                user : value,
                errors:{
                    ...prevstate.errors,
                    ...prevstate.errors.username,
                    username : true,
                }
            }))
        }
    }
    async handleCreateSelleer(){
        const {complete_name, password, user} = this.state
        if(complete_name && password && user){
            const { data } = await axios.post(global.BASE_URL+'auth/register',{
                nombre_completo : this.state.complete_name,
                user: this.state.user,
                rol:'seller',
                pass:this.state.password
            })

            if(data.status){
                this.props.changeSellerSection(this.props.show)
            }
            else{
                auth.forceLogOut()
            }
        }
        else{
            toast.error('Complete el formulario con los datos solicitados')
        }
    }
}
export default AddSeller