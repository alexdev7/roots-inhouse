import React, {Component} from 'react'
import axios from 'axios'
import global from 'api/global'
import auth from 'assets/auth'
import Navbar from 'components/Breadcrumb'
class Sellers extends Component{
    state={
        users:[],
        search:''
    }
    async componentDidMount(){
        const { data } = await axios.get(global.BASE_URL+'usuarios/seller',{
            headers:{
                'access-token' : auth.getToken()
            }
        })

        if(data.status){
            this.setState({
                users: data.users
            })
        }
        else{
            auth.forcedLogOut()
        }
    }
    noUsers=()=> <h1>No hay usuarios</h1>

    renderUsers = (user, index) => {
        if(user){
            return(
                <div key={index} className="table-row">
                    <div className="table-cell hover:bg-gray-300 text-gray-700 px-4 py-2 text-sm">
                        {user.nombre_completo}
                    </div>
                    <div className="table-cell hover:bg-gray-300 text-gray-700 px-4 py-2 text-sm">
                        {user.user}
                    </div>
                    <div className="table-cell hover:bg-gray-300 text-gray-700 px-4 py-2 text-sm">
                        {user.rol}
                    </div>
                    <div className="table-cell hover:bg-gray-300 text-gray-700 px-4 py-2 text-sm">
                        <button className="inline bg-red-500 py-1 rounded text-white px-3 text-md">eliminar</button>
                    </div>
                </div>
            )
        }
        return this.noUsers()
    }
    onchange = e => {
        this.setState({
            search : e.target.value
        })
    }
    render(){
        const {search} = this.state

        const filteredService = this.state.users.filter( user => {
            return user.nombre_completo.toLowerCase().indexOf(search.toLowerCase()) !== -1
        })

        return(
            <div className="h-full w-full bg-white px-3 py-2 rounded shadow-md">
                <Navbar>
                    <li><a className="text-blue cursor-pointer font-bold">Vendedores</a></li>
                </Navbar>
                <button onClick={()=>this.props.changeSellerSection(this.props.add)} className="py-1 px-3 rounded text-white bg-blue-500 my-2">agregar vendedor</button>
                <div className="flex flex-wrap py-4">
                    <div className="w-1/2">
                        <div class="relative">
                            <input autoComplete="off" onChange={ (event)=> this.onchange(event) } className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-10 text-gray-700 leading-tight focus:outline-none" placeholder="busca algo ..." id="inline-full-name" type="text" /> 
                            <div className="pointer-events-none absolute inset-y-0 left-0 flex items-center px-2 text-gray-700">
                                <span className="material-icons text-gray-600">
                                search
                                </span>
                            </div>
                        </div>
                        <div className="table w-full pt-2">
                            <div className="table-row-group">
                                <div className="table-row">
                                <div className="table-cell font-bold text-gray-700 px-4 py-2 text-sm cursor-pointer">Nombre</div>
                                <div className="table-cell font-bold text-gray-700 px-4 py-2 text-sm">Usuario</div>
                                <div className="table-cell font-bold text-gray-700 px-4 py-2 text-sm">Rol</div>
                                <div className="table-cell font-bold text-gray-700 px-4 py-2 text-sm">Acciones</div>
                                </div>
                                { filteredService.map((users, index) => {
                                    return this.renderUsers(users, index)
                                })}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default Sellers