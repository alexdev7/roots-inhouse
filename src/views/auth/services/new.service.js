import React, {Component} from 'react'
import Icon from '@material-ui/core/Icon'
import axios from 'axios'
import global from 'api/global'
import validate from 'validation/validator'
import messages from 'messages/services'
import { ToastContainer, toast } from 'react-toastify'
import auth from 'assets/auth'
import Rodal from 'rodal'
import 'rodal/lib/rodal.css';
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css';
import Navbar from 'components/Breadcrumb'
class NewService extends Component{
    
    state={
        services:[],
        search:'',
        name_service:'',
        selectedFile:'',
        file:'',
        status:'',
        visible:false,
        errors:{
            name:false,
            messagename:'',
            status:false,
            messagestatus:'',
        },
        loading_sections:true,
        showSections:false,
        selected:{
            name:'',
            status: '',
            sections:[]
        }
    }

    confirmDelete = (id) => {

        try {
            confirmAlert({
            title: <h1 className='font-bold'>Eliminar servicio</h1>,
                message: '¿Estas seguro de eliminar este servicio?',
                buttons: [
                {
                    label: 'Si',
                    onClick: () => this.delete(id)
                },
                {
                    label: 'No',
                    onClick: () => toast.warn('Operacion cancelada')
                }
                ]
            });
        } catch (error) {
            
        }
    };
    openModal(data){
        this.setState(prevstate=>({
            visible : true,
            selected:{
                ...prevstate.selected,
                ...prevstate.selected.name,
                name : data.nombre
            }
        }))

        this.getSections(data._id)
        
    }
    closeModal(){
        this.setState({
            visible:false
        })
    }
    async componentDidMount(){
        
        const res = await axios.get(global.BASE_URL + 'servicios-admin')
        this.setState({
            services : res.data.servicios
        })
    }

    async reload(){
        const res = await axios.get(global.BASE_URL + 'servicios-admin')
        this.setState({
            services : res.data.servicios
        })
    }

    async reloadSections(id){
        const res = await axios.get(global.BASE_URL+`servicios/${id}`)
        const service = res.data.servicio
        
        this.setState({
            sections : service.secciones
        })
    }

    renderSelected = async(id) => {
        this.setState({
            showSections:true
        })
        const res = await axios.get(global.BASE_URL+`servicios/${id}`)
        const service = res.data.servicio
        this.setState({
            sections : service.secciones
        })
    }

    noSections = () =>  <h1>Ese servicio no cuenta con secciones</h1>
    noServices = () =>  <h1>No se han encontrados servicios registrados</h1>
    loadingSections = () => <h1>Cargando secciones...</h1>

    

    async getSections(id){
        const { data } = await axios.get(global.BASE_URL+'servicios/'+id)
        this.setState(prevstate=>({
            selected:{
                ...prevstate.selected,
                ...prevstate.selected.sections,
                sections: data.servicio.secciones
            },
            loading_sections:false,
        }))        
    }    


    renderSections = (sections, index) => {
        return(
            <div className="flex flex-wrap">
                <div className="p-2 w-full">
                    <div key={index} className="px-3 border py-3 rounded shadow-lg flex justify-between border-gray-300">
                    <h1>{sections.nombre}</h1>   
                    </div>
                </div>
            </div>
        )
    }

    renderServices = (service, index) => {
        if(service){
            return(
                
                <div key={index} className="table-row">
                    <div className="table-cell hover:bg-gray-300 text-gray-700 px-4 py-2 text-sm">
                        {service.nombre}
                    </div>
                    <div className="table-cell hover:bg-gray-300 text-gray-700 px-4 py-2 text-sm">
                        <img className="h-40 w-64" src={service.imagen}/>
                    </div>
                    <div className="table-cell text-gray-700 px-4 py-2 text-sm">{service.status ? 'Activo' : 'Inactivo' }</div>
                    <div className="table-cell text-white flex justify-start px-4 py-2 text-sm">
                        <button onClick={ ()=> this.props.changeServicesSections(this.props.number, {
                            id:service._id,
                            name : service.nombre,
                            status : service.status,
                            secciones : service.secciones,
                            imagen : service.imagen
                        }) } className="py-2 bg-yellow-500 px-4 rounded">
                            Editar
                        </button>
                    </div>
                    <div className="table-cell text-white flex justify-start px-4 py-2 text-sm"> 
                    <button onClick={ ()=> this.confirmDelete(service._id) /*this.delete(service._id)*/ } className="py-2 bg-red-500 px-4 rounded">
                            Eliminar
                        </button>
                    </div>
                    
                </div>
            )
        }
        return this.noServices()
    }
        
    render(){
        const {search} = this.state

        const filteredService = this.state.services.filter( service => {
            return service.nombre.toLowerCase().indexOf(search.toLowerCase()) !== -1
        })

        const { name , messagename, status, messagestatus } = this.state.errors
        
        return(
            <div className="w-full overflow-auto pb-10 px-3 bg-white py-2 rounded shadow-md">
                <Navbar >
                    <li><a onClick={()=>this.props.changeSellerSection(this.props.show)} className="text-blue cursor-pointer font-bold">Servicios</a></li>
                </Navbar>
                <ToastContainer />
                <Rodal visible={this.state.visible} onClose={ ()=>this.closeModal()}>
                    <div className="w-full h-full overflow-scroll overflow-hidden">
                        <h1> Secciones del servicio seleccionado </h1>
                        <div className="py-2">
                            {
                                !this.state.loading_sections ? 
                                    this.state.selected.sections.length > 0 ? 
                                        this.state.selected.sections.map((section,index)=>{
                                            return this.renderSections(section, index)
                                        })
                                    : 
                                    this.noSections()
                                :
                                this.loadingSections()
                            }
                        </div>
                    </div>
                </Rodal>
                <div className="py-4">
                    <div className="h-32 w-40 px-3 flex items-center justify-center bg-gray-300">
                            <input onChange={(event)=>this.handleSelectImage(event.target.files[0])} type="file" id="files" className="hidden"/>
                            {
                                this.state.file ? 
                                    <img className="h-32 w-64" src={this.state.file}/>
                                : <label htmlFor="files">Selecciona una imagen</label>
                            }
                    </div>
                    {
                        this.state.file ?
                        <div className="py-4">
                            <input onChange={(event)=>this.handleSelectImage(event.target.files[0])} type="file" id="change_file" className="hidden"/>
                            <label htmlFor="change_file" className="py-2 px-3 text-white rounded bg-blue-500">cambiar</label>
                        </div> : 
                        ''
                    }
                    <div className="flex flex-wrap -mx-3 ">
                        <div className="sm:w-full md:w-1/3 py-6 px-3">
                            <input ref="service" onChange={ (event)=> this.handleNameService(event.target.value) } className="appearance-none block w-full text-gray-700 border-b-2 py-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-city" type="text" placeholder="Nombre de seccion"/>
                            <p className="text-red-600 text-xs italic py-2">
                                {
                                    !name ? messagename : '' 
                                }
                            </p>
                        </div>
                    </div>
                    <button onClick={ ()=> this.handleSaveService() } className="text-sm border-blue-600 bg-blue-500 text-white focus:outline-none rounded px-3 py-2">Agregar</button>
                </div>
                <div className="flex flex-wrap py-4">
                        <div className="w-1/2">
                            <div class="relative">
                                <input autoComplete="off" onChange={ (event)=> this.onchange(event) } className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-10 text-gray-700 leading-tight focus:outline-none" placeholder="busca algo ..." id="inline-full-name" type="text" /> 
                                <div className="pointer-events-none absolute inset-y-0 left-0 flex items-center px-2 text-gray-700">
                                    <span className="material-icons text-gray-600">
                                    search
                                    </span>
                                </div>
                            </div>
                            <div className="table w-full pt-2">
                                <div className="table-row-group">
                                    <div className="table-row">
                                    <div className="table-cell font-bold text-gray-700 px-4 py-2 text-sm cursor-pointer">Nombre</div>
                                    <div className="table-cell font-bold text-gray-700 px-4 py-2 text-sm cursor-pointer">Icono</div>
                                    <div className="table-cell font-bold text-gray-700 px-4 py-2 text-sm">Estado</div>
                                    </div>
                                    { filteredService.map((service, index) => {
                                        return this.renderServices(service, index)
                                    })}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        )
    }

    handleNameService(event){
        if(!validate.isEmpty(event)){
            this.setState(prevstate =>({
                errors:{
                    ...prevstate.errors,
                    ...prevstate.errors.name,
                    name : false,
                    ...prevstate.errors.messagename,
                    messagename : messages.required
                }
            }))
        }   
        else if(!validate.isValid(event,'string')){
            this.setState(prevstate =>({
                errors:{
                    ...prevstate.errors,
                    ...prevstate.errors.name,
                    name : false,
                    ...prevstate.errors.messagename,
                    messagename : messages.onlyLetters
                }
            }))
        }
        else{
            this.setState(prevstate =>({
                name_service : event,
                errors:{
                    ...prevstate.errors,
                    ...prevstate.errors.name,
                    name : true,
                }
            }))
        }
    }

    handleSelectImage(value){
        this.setState({
            file: URL.createObjectURL(value),
            selectedFile : value
        })
    }

    handleComboSelected(value){
        if( !value || value === 0 || value === '0' ){
            this.setState(prevstate =>({
                errors:{
                    ...prevstate.errors,
                    ...prevstate.errors.status,
                    status : false,
                    ...prevstate.errors.messagestatus,
                    messagestatus : 'Seleccione un estado de servicio'
                }
            }))
        }
        else{
            this.setState(prevstate =>({
                status : value,
                errors:{
                    ...prevstate.errors,
                    ...prevstate.errors.status,
                    status : true,
                    
                }
            }))
        }
    }

    async handleSaveService(){
        
        const {name_service} = this.state
        
        const {name} = this.state.errors   
        if(name){
            const form = new FormData();
            form.append('imagen', this.state.selectedFile)
            form.append("nombre", this.state.name_service);

            const res = await axios.post(global.BASE_URL+'servicios',form,{
                headers: { 'access-token': auth.getToken() }
            })
            
            if(res.data.status){

                toast.success('Seccion agregada exitosamente');
                this.reload()

                this.setState({
                    name_service: '',
                    status:'',
                    file:'',
                    selectedFile:''
                })

                this.refs.service.value = ''
            }
            else{
                localStorage.removeItem('session')
                window.location.href='/login'
            }
        }
        else{
            toast.error('No puedes agregar una sección todavia!');
        }
    }

    onchange = e => {
        this.setState({
            search : e.target.value
        })
    }

   

    async delete(id){
        const res = await axios.delete(global.BASE_URL+`servicios/${id}`,{
            headers: { 'access-token': auth.getToken() }
        })

        if(res.data.status){
            toast.error('Servicio eliminado correctamente')
            this.reload()
        }
        else{
            localStorage.removeItem('session')
            window.location.href='/login'
        }
        
    }
} 

export default NewService