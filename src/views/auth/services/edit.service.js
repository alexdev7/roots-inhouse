import React, {Component} from 'react'
import { changeServicesSections } from '../../../redux/actions/app.actions'
import { connect } from 'react-redux'
import Icon from '@material-ui/core/Icon'
import axios from 'axios'
import global from 'api/global'
import validate from 'validation/validator'
import messages from 'messages/services'
import auth from 'assets/auth'
import { ToastContainer, toast } from 'react-toastify'
import Navbar from 'components/Breadcrumb'
class EditService extends Component{
    state = {
        _id : '',
        name : '',
        status : '',
        sections : [],
        file:'',
        selectedFile:'',
        errors:{
            name:false,
            messagename:'',
            status:false,
            messagestatus:''
        }
    }
    
    componentDidMount(){
        
        const data = JSON.stringify(this.props.page.parameter_service)
        const display = JSON.parse(data)
        
        this.setState(prevstate => ({
            ...prevstate.errors.name = true,
            ...prevstate.errors.status = true,
        }))

        this.setState({
            _id : display.id,
            name : display.name,
            status : display.status,
            sections : display.secciones,
            file: display.imagen,
        })

    }

    handleSelectImage(value){
        this.setState({
            file: URL.createObjectURL(value),
            selectedFile : value
        })
    }
    
    render(){
        const data = JSON.stringify(this.props.page.parameter_service)
        const display = JSON.parse(data)
        const { name, messagename, status, messagestatus } = this.state.errors
        return(
            <div className="w-full bg-white px-3 py-2 rounded shadow-md">
                
                <ToastContainer />
                <Navbar child="Editar servicio">
                    <li><a onClick={()=>this.props.changeServicesSections(this.props.number)} className="text-blue underline cursor-pointer font-bold">Servicios</a></li>
                </Navbar>
                <div className="h-32 w-40 px-3 flex items-center justify-center bg-gray-300">
                    <input onChange={(event)=>this.handleSelectImage(event.target.files[0])} type="file" id="files" className="hidden"/>
                    {
                        this.state.file ? 
                            <img className="h-32 w-64" src={this.state.file}/>
                        : <label htmlFor="files">Selecciona una imagen</label>
                    }
                </div>
                {
                    this.state.file ?
                    <div className="py-4">
                        <input onChange={(event)=>this.handleSelectImage(event.target.files[0])} type="file" id="change_file" className="hidden"/>
                        <label htmlFor="change_file" className="py-2 px-3 text-white rounded bg-blue-500">cambiar</label>
                    </div> : 
                    ''
                }
                <div className="flex flex-wrap -mx-3">
                    
                    <div className="sm:w-full md:w-1/3 py-6 px-3">
                        <input defaultValue = {display.name}  onChange={ (event)=> this.handleEditServiceName(event.target.value) } ref="service" className="appearance-none block w-full text-gray-700 border-b-2 py-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-city" type="text" placeholder="Nombre de seccion"/>
                        <p className="text-red-600 text-xs italic py-2">
                            {
                                !name ? messagename : '' 
                            }
                        </p>
                    </div>
                    <div className="w-1/2 sm:w-full md:1/3 py-6 px-3">
                    <div className="relative">
                        <select defaultValue={display.status} onChange={ (event) => this.handleComboSelected(event.target.value) } className="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-state">
                            <option value="0">Selecciona un estado</option>
                            <option value="true">Activo</option>
                            <option value="false">Inactivo</option>
                        </select>
                        <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                            <svg className="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
                        </div>
                    </div>
                    <p className="text-red-600 text-xs italic py-2">
                        {
                            !status ? messagestatus : ''
                        }
                    </p>
                    </div>
                </div>
                <button onClick={ ()=>this.edit() } className="text-sm border-blue-600 bg-yellow-500 text-white focus:outline-none rounded px-3 py-2">Editar</button>

            </div>
        )
    }

    handleEditServiceName(event){
        if(!validate.isEmpty(event)){
            this.setState(prevstate =>({
                errors:{
                    ...prevstate.errors,
                    ...prevstate.errors.name,
                    name : false,
                    ...prevstate.errors.messagename,
                    messagename : messages.required
                }
            }))
        }   
        else if(!validate.isValid(event,'string')){
            this.setState(prevstate =>({
                errors:{
                    ...prevstate.errors,
                    ...prevstate.errors.name,
                    name : false,
                    ...prevstate.errors.messagename,
                    messagename : messages.onlyLetters
                }
            }))
        }
        else{
            this.setState(prevstate =>({
                name : event,
                errors:{
                    ...prevstate.errors,
                    ...prevstate.errors.name,
                    name : true,
                }
            }))
        }
    }
    handleComboSelected(value){
        if( !value || value === 0 || value === '0' ){
            this.setState(prevstate =>({
                errors:{
                    ...prevstate.errors,
                    ...prevstate.errors.status,
                    status : false,
                    ...prevstate.errors.messagestatus,
                    messagestatus : 'Seleccione un estado de servicio'
                }
            }))
        }
        else{
            this.setState(prevstate =>({
                status : value,
                errors:{
                    ...prevstate.errors,
                    ...prevstate.errors.status,
                    status : true,
                }
            }))
        }
    }
    
    async edit(){
        
        const { changeServicesSections, number } = this.props
        const {name, status} = this.state.errors
        
        if(name && status){
            
            const res = await axios.put(global.BASE_URL+'servicios',{
                id : this.state._id,
                nombre : this.state.name,
                status : this.state.status,
                secciones : this.state.sections,
                imagen : this.state.selectedFile
            },{
                headers : {
                    'access-token' : auth.getToken()
                }
            })
      
            if(res.data.status){
                changeServicesSections(number)
            }
            else{
                localStorage.removeItem('session')
                window.location.href='/login'
            }
        }
        else{
            toast.error('No hay datos para editar este servicio!')
        }
    }

}
const mapStateToProps = (state) => {
    return {
        page : state.page
    }
}

const dispatchStateToProps = {
    changeServicesSections
}
export default connect(mapStateToProps, dispatchStateToProps)(EditService)