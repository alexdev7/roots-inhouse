import React, {Component} from 'react'
import axios from 'axios'
import global from 'api/global'
import auth from 'assets/auth'
import Navbar from 'components/Breadcrumb'

//importas el componente (no importa si no lleva el .js)
import Ejemplo from '../../../views/example'

class Cotizations extends Component{
    state={
        cotizations:[],
        search:''
    }
    async componentDidMount(){
        if(auth.isAuthenticatedSeller()){
            const {data} = await axios.get(global.BASE_URL+`cotizador/${auth.getId()}`,{
                headers:{
                    'access-token' : auth.getToken()
                }
            })

            if(data.status) { 
                this.setState({
                    cotizations : data.cotizador
                })
            }
            else{
                auth.forcedLogOut()
            }
        }
        else{
            const {data} = await axios.get(global.BASE_URL+`cotizador-admin`,{
                headers:{
                    'access-token' : auth.getToken()
                }
            })

            if(data.status) { 
                this.setState({
                    cotizations : data.cotizador
                })
            }
            else{
                auth.forcedLogOut()
            }
        }

        

        
    }
    renderCotizations = (cotization, index) => {
        return (
            <div key={index} className="table-row">
                <div className="table-cell text-gray-700 px-4 py-2 text-sm">
                    {cotization.solicita.empresa}
                </div>
                <div className="table-cell text-gray-700 px-4 py-2 text-sm">
                    {cotization.solicita.email}
                </div>
                <div className="table-cell text-gray-700 px-4 py-2 text-sm">
                    {cotization.precio_total}
                </div>
                <div className="table-cell text-gray-700 px-4 py-2 text-sm">
                    {cotization.oferta}
                </div>
                <div className="table-cell text-gray-700 px-4 py-2 text-sm">
                    {
                        cotization.proyecto ? 
                        cotization.proyecto.nombre_proyecto : 'Proyecto no definido'
                    }
                </div>
                <div className="table-cell text-gray-700 px-4 py-2 text-sm">
                    <button onClick={()=>this.props.changeCotizationView(this.props.view, {
                        cotization:cotization,
                        options : cotization.opciones
                    })} className="py-1 px-3 border border-blue-500 bg-white text-blue-500 rounded">Ver cotización</button>
                </div>
            </div>
        )
    }
    onchange = e => {
        this.setState({
            search : e.target.value
        })
    }
    render(){
        const {search} = this.state

        const filteredCotization = this.state.cotizations.filter( cotization => {
            return cotization.solicita.empresa.toLowerCase().indexOf(search.toLowerCase()) !== -1 || cotization.solicita.email.toUpperCase().indexOf(search.toUpperCase()) !== -1
        })
        return(
            <div className="h-full w-full bg-white px-3 py-2 rounded shadow-md">
                <Navbar parent='Cotizaciones'>
                    <li><a onClick={()=>this.props.changeCotizationView(this.props.show)} className="text-blue cursor-pointer font-bold">Cotizaciones</a></li>
                </Navbar>
                <div className="flex flex-wrap py-4">
                    <div className="w-full pb-4">
                        <div class="relative">
                            <input autoComplete="off" onChange={ (event)=> this.onchange(event) } className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-10 text-gray-700 leading-tight focus:outline-none" placeholder="busca algo ..." id="inline-full-name" type="text" /> 
                            <div className="pointer-events-none absolute inset-y-0 left-0 flex items-center px-2 text-gray-700">
                                <span className="material-icons text-gray-600">
                                search
                                </span>
                            </div>
                        </div>
                        <div className="table w-full pt-2">
                            <div className="table-row-group">
                                <div className="table-row">
                                    <div className="table-cell font-bold text-gray-700 px-4 py-2 text-sm cursor-pointer">Solicita</div>
                                    <div className="table-cell font-bold text-gray-700 px-4 py-2 text-sm cursor-pointer">Empresa</div>
                                    <div className="table-cell font-bold text-gray-700 px-4 py-2 text-sm cursor-pointer">Precio total</div>
                                    <div className="table-cell font-bold text-gray-700 px-4 py-2 text-sm cursor-pointer">Oferta</div>
                                    <div className="table-cell font-bold text-gray-700 px-4 py-2 text-sm cursor-pointer">Proyecto</div>
                                    <div className="table-cell font-bold text-gray-700 px-4 py-2 text-sm">Acciones</div>
                                </div>
                                {
                                    filteredCotization.map((cotizations, index)=>{
                                        return this.renderCotizations(cotizations, index)
                                    })
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Cotizations