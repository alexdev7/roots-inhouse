import React, {Component} from 'react'
import {connect} from 'react-redux'
import Navbar from 'components/Breadcrumb'
class ViewCotization extends Component {
    state = {
        options : [],
        cotization:''
    }
    componentDidMount(){
        this.setState({
            options : this.props.page.parameter_cotization.options,
            cotization:this.props.page.parameter_cotization.cotization
        })
    }
    renderOptions = (option, index) => {
        return(
            <div key={index} className="w-full border-black border-b-2">
                <div className="px-2 py-2">
                    <h1 className="font-bold">{option.nombre}</h1>
                    <p>Precio: {option.precio}</p>
                    <p>Tiempo: {option.tiempo} Hras</p>
                </div>
            </div> 
        )
    }
    render(){
        
        return (
            <div className="h-full w-full pb-10 bg-white px-3 py-2 rounded shadow-md">
                <Navbar parent='Cotizaciones' child='Ver cotizacion'>
                    <li><a onClick={()=>this.props.changeCotizationView(this.props.list)} className="text-blue underline cursor-pointer font-bold">Cotizaciones</a></li>
                </Navbar>
                <div className="w-full rounded bg-blue-600 text-white py-3 my-3 px-3 font-bold">
                    <p className="mt-3">Tiempo de realizacion: {this.state.cotization.tiempo_realizacion}</p>
                    <p className="mt-3">Costo proyecto: ${this.state.cotization.precio_total}</p>
                    <p className="mt-3">Oferta: ${this.state.cotization.oferta}</p>
                </div>
                <div className="w-1/2 pb-10">
                    <h1 className="my-5 text-lg">Opciones seleccionadas:</h1>
                    <div className="w-full rounded border-t border-r border-l border-black">
                        {
                            this.state.options.map( (options, index) =>{
                                return this.renderOptions(options, index)
                            } )
                        }                        
                    </div>
                </div>
            </div>
        )
    }
}
const mapStateToProps = state => {
    return {
        page : state.page
    }
}
export default connect (mapStateToProps)(ViewCotization)