import React , { Component } from 'react'

import { changeSectionOfSections } from '../../redux/actions/app.actions'

import { connect } from 'react-redux'

import Show from './sections/show'
import New from './sections/new.section'
import Edit from './sections/edit.section'
import AddSectionToService from './sections/addSectionToService'
import OptionSection from './sections/new-option'
import ShowOptionsSection from './sections/view-options'
import EditOption from './sections/edit-option'
class Sections extends Component{
    constructor(props){
        super(props)
        
    }
    changeSectionOfSections = (number, parameter = null) => {
        this.props.changeSectionOfSections(number, parameter)
    }
    renderView(){
        switch(this.props.page.sections){
            case 0:
                return <Show 
                changeSectionOfSections = { this.changeSectionOfSections } 
                add={ 1 } services={ 3 } edit={ 2 } options={ 4 } />
            case 1:
                return <New 
                changeSectionOfSections = { this.changeSectionOfSections } 
                show={ 0 } />
            case 2:
                return <Edit 
                changeSectionOfSections = { this.changeSectionOfSections } 
                show={ 0 }/>
            case 3:
                return <AddSectionToService  
                changeSectionOfSections = { this.changeSectionOfSections } 
                show={ 0 }
                />
            case 4:
                return <ShowOptionsSection changeSectionOfSections = { this.changeSectionOfSections } 
                add={ 5 } edit={ 6 } />
            case 5:
                return <OptionSection changeSectionOfSections = { this.changeSectionOfSections } 
                show_options={ 4 }/>
            case 6:
                return <EditOption changeSectionOfSections = { this.changeSectionOfSections } 
                show_options={ 4 }/>
            default:
                return <Show changeSectionOfSections = { this.changeSectionOfSections } 
                add={ 1 } services={ 3 } edit={ 2 } options={4}/>

        }
    }
    render()
    {
        return(
            
        <div className="p-3">
            { this.renderView() }   
        </div>
                  
        )
    }
}

const mapStateToProps = (state) => {
    return {
        page : state.page
    }
}
const dispatchStateToProps = {
    changeSectionOfSections
}

export default connect(mapStateToProps, dispatchStateToProps)(Sections)