import React , { Component } from 'react'

import { changeSectionOptions } from '../../redux/actions/app.actions'

import { connect } from 'react-redux'

import Show from './options/show'

class Options extends Component{
    constructor(props){
        super(props)
    }

    changeSectionOptions = (number) => {
        this.props.changeSectionOptions(number)
    }

    renderView (){
        switch(this.props.page.options){
            case 0:
                return <Show changeSectionOptions = { this.changeSectionOptions }  add = {1}/>   
            default :
                return <Show changeSectionOptions = { this.changeSectionOptions }  add = {1}/>
            
        }
    }
    render()
    {
        return(
            <div className="p-8">
                { this.renderView() }
            </div>
        )
    }
}



const mapStateToProps = (state) => {
    return {
        page : state.page
    }
}

const dispatchStateToProps = {
    changeSectionOptions
}

export default connect(mapStateToProps, dispatchStateToProps)(Options) 