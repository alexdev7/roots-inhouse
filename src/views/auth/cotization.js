import React, {Component} from 'react'
import {connect} from 'react-redux'

import Show from './cotizations/cotizations'
import ViewCotization from './cotizations/view-cotization'

import {changeCotizationView} from '../../redux/actions/app.actions'
class Cotization extends Component{
    changeCotizationView = (index, parameter = null) => {
        this.props.changeCotizationView(index, parameter)
    }
    renderView(){
        switch(this.props.page.cotization){
            case 0:
                return <Show changeCotizationView={this.changeCotizationView}  view={1}/>
            case 1:
                return <ViewCotization changeCotizationView={this.changeCotizationView} list={0}/>
            default:
                return <Show changeCotizationView={this.changeCotizationView} view={1}/>

        }
    }
    render()
    {
        return(
            <div className="p-3">
                { this.renderView() }   
            </div>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        page : state.page
    }
}
const dispatchStateToProps = {
    changeCotizationView
}

export default connect(mapStateToProps, dispatchStateToProps)(Cotization)