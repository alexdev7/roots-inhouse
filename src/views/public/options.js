import React, {Component} from 'react'
import Controls from 'components/controls'
import SectionCard from 'components/sectionCard'
import global from 'api/global'
import axios from 'axios'
import OptionsCard from 'components/optionsCard'
import {connect} from 'react-redux'
import { publicChangePage } from '../../redux/actions/app.actions'

class OptionsServices extends Component{
    state= {
        options : []
    }
    async componentDidMount(){
        const res = await axios.get(global.BASE_URL+`secciones/${ this.props.page.parameter_selected }`)
        this.setState({
            options : res.data.secciones.opciones
        })
    }
    render(){
        
        return(
            <div className="w-full h-full">
                <div className="flex mt-32 text-2xl justify-center">
                    <h1 className="text-white">Opciones</h1>
                </div>
                <div className="flex flex-wrap md:px-64 mt-12">
                    {  
                       this.state.options ? this.state.options.map( option => <OptionsCard number={2} key={ option._id } { ...option }/> ) :
                       
                       <div className="w-full md:px-32 p-2">
                            <div className="bg-white shadow-xl hover:bg-blue-400 hover:text-white rounded hover:shadow-2xl py-4 text-center text-blue-600 p-2 cursor-default">
                                <a> No hay opciones disponibles </a>
                            </div>
                        </div>
                    }
                </div>
                <Controls number={1}/>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        page : state.page
    }
}

const dispatchStateToProps = {
    publicChangePage
}

export default connect(mapStateToProps, dispatchStateToProps)(OptionsServices)