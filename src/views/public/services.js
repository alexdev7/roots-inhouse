import React, {Component} from 'react'
import logo from 'assets/logoroots.png'
import SectionCard from 'components/sectionCard'
import axios from 'axios'
import global from 'api/global'
import Controls from 'components/controls'
import CircularProgress from '@material-ui/core/CircularProgress'

import {addDataPublic} from '../../redux/actions/app.actions'
import { connect } from 'react-redux'

import cookie from 'react-cookies'
import { googleTranslate } from '../../utils/googleTranslate'

class Services extends Component{
    state={
        services : [],
        language: cookie.load("language") ? cookie.load("language") : "es",
        
        show:true,
        title:cookie.load("title")
        ? cookie.load("title")
        : "Servicios",
    }
    async componentDidMount(){

        const res = await axios.get(global.BASE_URL+'servicios') 
        this.setState({
            services : res.data.servicios,
            show:false
        })
        
        const { language , title} = this.state
        
        let cookieLanguage = cookie.load("language");
        let transQuestion = "";

        const translating_phrase1 = transQuestion => {
            this.setState({ title: transQuestion });
            cookie.save("title", transQuestion, { path: "/" });
        };

        googleTranslate.translate(title, language, function(err, translation) {
            transQuestion = translation.translatedText;
            translating_phrase1(transQuestion);
        });

        this.setState({ language });
        cookie.save("language", language, { path: "/" });

    }

    renderServices = (service, index) => {

        if(cookie.load('language')){
            let language = cookie.load("language") ? cookie.load('language') : '';
            let transQuestion = "";

            const translating_phrase1 = transQuestion => {
                service.nombre = transQuestion
                cookie.save("name", service.nombre, { path: "/" });
            };

            googleTranslate.translate(service.nombre, language, function(err, translation) {
                transQuestion = translation.translatedText;
                translating_phrase1(transQuestion);
            });

            cookie.save("language", language, { path: "/" });
        }
        
        return (
            <div key={index} onClick={ ()=> {
                    this.props.publicChangePage(this.props.sections, service._id)
                    this.props.addDataPublic(
                        { service : service.nombre})
                } } className="w-full sm:w-full md:w-1/3 lg:w-1/3 p-5">
                <div className="py-10 rounded text-center bg-transparent p-2 border border-blue-500 transition duration-500 ease-in-out transform hover:-translate-y-1 hover:scale-110 text-blue-500">
                    <div className="flex justify-center">
                        <img className="h-32" src={service.imagen} />
                    </div>
                <h1>{service.nombre}</h1>
                </div>
            </div>
        )
    }
    noService = () => {
        return (
           <div className="flex justify-center w-full">
                <h1 className="text-white text-lg">No hay servicios disponibles.</h1>
           </div>
        )
    }

    render(){
        if(this.state.show){
            return (
                <div className="flex text-white justify-center pt-40">
                    <CircularProgress color="inherit"/>
                </div>
            )
        }
        else{
            
            return (
                <div className="w-full h-full">
                    <div className="w-full pt-10 sm:pt-0 md:pt-40 lg:pt-40 flex justify-center">
                    <h1 className="text-2xl text-blue-500">{this.state.title}</h1>
                    </div>
                    <div className="flex flex-wrap py-4 sm:flex sm:flex-wrap md:mx-64 sm:mx-0">
                        {
                            this.state.services.length > 0 ?
                            this.state.services.map((service, index) => {
                                return this.renderServices(service, index)
                            }) : this.noService()
                        }
                    </div>
                    <Controls number={0}/>
                </div>
    
            )
        }
    }
}
const mapStateToProps = (state) => {
    return {
        page : state.page
    }
}

const dispatchStateToProps = {
    addDataPublic
}
export default connect(mapStateToProps,dispatchStateToProps)(Services)

