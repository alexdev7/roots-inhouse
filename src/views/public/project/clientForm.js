import React, { Component, useState } from 'react';
import pic1 from '../../../assets/pic1.svg'
import pic2 from '../../../assets/pic2.svg'
import pic3 from '../../../assets/pic3.svg'
import pic4 from '../../../assets/pic4.svg'
import pic5 from '../../../assets/pic5.svg'
import Slide from 'react-reveal/Slide'
import PhoneInput from 'react-phone-input-2'
import Icon from '../../../components/IconComponent'
import 'react-phone-input-2/lib/style.css'
import { connect } from 'react-redux'
import { publicChangePage, changeText } from '../../../redux/actions/app.actions'

class ClientForm extends Component {
    
    state={
        phone : ''
    }
    setValue=(value)=>{
        this.setState({
            phone : value
        })
    }
    render(){
        
        return(
            <Slide left>
                <div className="w-full h-screen p-10">
                    <div className="bg-white overflow-auto h-full px-10 px-3">
                        <button onClick={()=>this.props.publicChangePage(0)} className="py-3 px-2 outline-none focus:outline-none my-5 text-black flex justify-center"> <Icon className="text-black" IconName="keyboard_backspace"/>  <span className="ml-5">Volver a iniciar</span></button>
                        <h1 className="text-2xl font-bold">Para terminar, quisieramos saber más sobre ti!</h1>
                        <div className="w-full flex flex-wrap sm:flex sm:flex-wrap -mx-3 sm:-mx-3 py-10 sm:px-40 pb-10">
                            <div className="w-full md:w-1/2 px-3 mb-6 md:mb-2">
                                <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-first-name">
                                    Nombres completos
                                </label>
                                <input className="appearance-none block w-full bg-gray-200 text-gray-700 border border-red-500 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" id="grid-first-name" type="text" placeholder="Jane"/>
                                <p className="text-red-500 text-xs italic">Please fill out this field.</p>
                            </div>
                            <div className="w-full md:w-1/2 px-3 mb-6 md:mb-2">
                                <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-first-name">
                                    Email
                                </label>
                                <input className="appearance-none block w-full bg-gray-200 text-gray-700 border border-red-500 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" id="grid-first-name" type="text" placeholder="Jane"/>
                                <p className="text-red-500 text-xs italic">Please fill out this field.</p>
                            </div>
                            <div className="w-full sm:w-full md:w-1/2 px-3 mb-6 md:mb-2">
                                <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-first-name">
                                    Telefono
                                </label>
                                
                                <PhoneInput 
                                inputStyle={{
                                    width:220
                                }}
                                value={this.state.phone}
                                onChange={phone => this.setState({ phone })}
                                placeholder="Digite su numero"
                                
                                />
                                <p className="text-red-500 text-xs italic">Please fill out this field.</p>
                            </div>
                            <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                                <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-first-name">
                                    Nombre de la empresa
                                </label>
                                <input className="appearance-none block w-full bg-gray-200 text-gray-700 border border-red-500 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" id="grid-first-name" type="text" placeholder="Jane"/>
                                <p className="text-red-500 text-xs italic">Please fill out this field.</p>
                            </div>
                        </div>
                        <div className="w-full">
                            <h1 className="text-lg">¿Que tipo de empresa eres?</h1>
                            <div className="flex w-full flex-wrap md:justify-center text-white uppercase mt-5">
                            <div className="w-full md:w-1/6 p-3">
                                    <div className="bg-blue-500 py-2 rounded">
                                    <img src={pic1} className="w-full h-20"/>
                                        <div className="py-4 flex justify-center">
                                            <h1>Soy particular</h1>
                                        </div>
                                    </div>
                                </div>
                                <div className="w-full md:w-1/6 p-3">
                                    <div className="bg-blue-500 py-2 rounded">
                                    <img src={pic2} className="w-full h-20"/>
                                        <div className="py-4 flex justify-center">
                                            <h1>StartUp</h1>
                                        </div>
                                    </div>
                                </div>
                                <div className="w-full md:w-1/6 p-3">
                                    <div className="bg-blue-500 py-2 rounded">
                                    <img src={pic3} className="w-full h-20"/>
                                        <div className="py-4 flex justify-center">
                                        <h1>Pyme</h1>
                                        </div>
                                    </div>
                                </div>
                                <div className="w-full md:w-1/6 p-3">
                                    <div className="bg-blue-500 py-2 rounded">
                                    <img src={pic4} className="w-full h-20"/>
                                        <div className="py-4 flex justify-center">
                                            <h1>Agencia</h1>
                                        </div>
                                    </div>
                                </div>
                                <div className="w-full md:w-1/6 p-3">
                                    <div className="bg-blue-500 py-2 rounded">
                                    <img src={pic5} className="w-full h-20"/>
                                        <div className="py-4 flex justify-center">
                                          <h1>Gran empresa</h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="w-full flex justify-center py-10">
                                <button className="px-6 md:px-24 py-2 bg-gray-200 text-gray-600 shadow-md ">Finalizar</button>
                        </div>
                    </div>
                </div>
            </Slide>
            )
    }
}


const mapStateToProps = (state) => {
    return{
        page : state.page
    }
}

const dispatchStateToProps = {
    publicChangePage,
    changeText
}

export default connect(mapStateToProps, dispatchStateToProps)(ClientForm)