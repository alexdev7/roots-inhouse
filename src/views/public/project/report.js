import React, {Component} from 'react'
import {connect} from 'react-redux'
import {publicChangePage, resetHistoryOptions, resetData} from '../../../redux/actions/app.actions'
import axios from 'axios'
import global from 'api/global'
import auth from 'assets/auth'
import cookie from 'react-cookies'
import { googleTranslate } from '../../../utils/googleTranslate'
import { ToastContainer, toast } from 'react-toastify'
var total = 0
class Report extends Component{
    state= {
        sections : [],
        service : '',
        options : [],
        prices:[],
        price_project: '',
        show:false,
        offer:'',
        isCompleted:false,

        name:'',
        lastname:'',
        email:'',

        title_cotization:'Cotizacion',
        service_selected:'Servicio seleccionado: ',
        message:'Ponte en contacto con nosotros',
        time_cotization : 'El proyecto cotizado estaria realizado en un estimado de: ',
        days_text : 'días',
        hours_work:'Horas de realización',

        name_label:'Nombre',
        lastname_label:'Apellido',
        email_label:'Email',
        button_send:'Entendido',
        button_create_proyect:'Crear proyecto',
        button_restart:'Volver a iniciar',
        price : 'El estimado es:',
        offer_label : 'Hacer oferta',

        placeholder_name : 'Nombre',
        placeholder_lastname : 'Apellido',
        placeholder_email:'Email',
        placeholder_offer:'Digite una oferta',

        toast_text:'Su cotizacion se ha enviado satisfactoriamente',

        days:'',
        hours:'',
        
    }
    async componentDidMount(){

        var sections = []
        var options = []
        var prices = []

        var time = 0
        var days = 0
        
        this.props.page.data.map(info=>{
            if(info.sections){

                prices.push(info.sections.option.precio)
                time += info.sections.option.tiempo
                sections.push(info.sections.section)
                
                options.push(info.sections.option._id)
            }
        })
        
        for(var i = 24; i<time; i+=24){
            days++
        }

        total = prices.reduce((a, b) => { return a + b; }, 0);
        
        this.setState({
            prices : prices,
            options : options,
            sections : sections,
            service : this.props.page.data[0].service,
            days : days,
            hours : time,
            price_project : total
        })

        

        if(cookie.load('language')){
            const language = cookie.load('language')
            googleTranslate.translate(this.state.title_cotization, language, (err, translation) => {
                this.setState({
                    title_cotization : translation.translatedText
                })
            });
    
            googleTranslate.translate(this.state.service_selected, language, (err, translation) => {
                this.setState({
                    service_selected : translation.translatedText
                })
            });
            googleTranslate.translate(this.state.price, language, (err, translation) => {
                this.setState({
                    price : translation.translatedText
                })
            });
            
           
            
            googleTranslate.translate(this.state.button_send, language, (err, translation) => {
                this.setState({
                    button_send : translation.translatedText
                })
            });
    
            googleTranslate.translate(this.state.button_create_proyect, language, (err, translation) => {
                this.setState({
                    button_create_proyect : translation.translatedText
                })
            });
    
            googleTranslate.translate(this.state.button_restart, language, (err, translation) => {
                this.setState({
                    button_restart : translation.translatedText
                })
            });

            googleTranslate.translate(this.state.toast_text, language, (err, translation) => {
                this.setState({
                    toast_text : translation.translatedText
                })
            });

            googleTranslate.translate(this.state.time_cotization, language, (err, translation) => {
                this.setState({
                    time_cotization : translation.translatedText
                })
            });

            googleTranslate.translate(this.state.days_text, language, (err, translation) => {
                this.setState({
                    days_text : translation.translatedText
                })
            });

            googleTranslate.translate(this.state.hours_work, language, (err, translation) => {
                this.setState({
                    hours_work : translation.translatedText
                })
            });

            googleTranslate.translate(this.state.offer_label, language, (err, translation) => {
                this.setState({
                    offer_label : translation.translatedText
                })
            });
            googleTranslate.translate(this.state.placeholder_offer, language, (err, translation) => {
                this.setState({
                    placeholder_offer : translation.translatedText
                })
            });
        }

        
        
    }
    openModal = () => {
        this.props.publicChangePage(3)
    }
    closeModal = () =>{
        this.setState({
            show : false
        })
    }
   
    handleName = (event) => {
        this.setState({
            name : event.target.value
        })
    }
    handleLastname = (event) => {
        this.setState({
            enterprise : event.target.value
        })
    }
    handleEmail = (event) => {
        this.setState({
            email : event.target.value
        })
    }
    handleOffer = (event) => {
        this.setState({
            offer : event.target.value
        })
    }

    handleSaveCotization = async () => {
       
        if(auth.isAuthenticatedSeller()){
            const res = await axios.post(global.BASE_URL+'cotizador',{
                opciones : this.state.options,
                secciones : this.state.sections,
                empresa : this.props.enterprise,
                email : this.props.email,
                oferta : this.state.offer ? this.state.offer : 0,
                precio_total : this.state.price_project,
                tiempo_realizacion: this.state.days +' '+this.state.days_text,
                proyecto:this.props.project_created,
                usuario: auth.getId()
            })
    
            if(res.data){
                this.setState({
                    isCompleted : true
                })
            }
            else{
                toast.warn(res.data)
            }
        }
        else{
            const res = await axios.post(global.BASE_URL+'cotizador',{
                opciones : this.state.options,
                secciones : this.state.sections,
                empresa : this.props.enterprise,
                email : this.props.email,
                oferta : this.state.offer ? this.state.offer : 0,
                precio_total : this.state.price_project,
                tiempo_realizacion: this.state.days +' '+this.state.days_text,
                proyecto:this.props.project_created,
            })
    
            if(res.data){
                this.setState({
                    isCompleted : true
                })
            }
            else{
                toast.warn(res.data)
            }
        }
    }
    render(){
        return(
            <div className="w-full h-full px-5 md:px-64"> 
                <ToastContainer />
                <div className="px-5 md:px-24 h-full py-10">
                    <div className="h-full rounded-rounded_roots_lg" style={{
                        backgroundColor : '#353434'
                    }}>
                        <div className="text-center py-10">
                            <h1 className="text-2xl font-bold text-white font-big_bold_text">{ this.state.title_cotization }</h1>
                        </div>
                        <div className="px-10 py-1">
                            <h1 className="text-blue-600 text-lg font-bold">{this.state.service_selected} {this.state.service}</h1>
                        </div>
                        {
                            auth.isAuthenticatedSeller() ? 
                                !this.state.isCompleted ? 
                                <div className="px-10 py-1">
                                    <p className="text-blue-600 font-bold text-2xl">
                                        {this.state.price} ${total}
                                    </p>
                                    <p className="text-blue-600 font-bold text-2xl">
                                        {this.state.hours_work}: {this.state.hours}
                                    </p>
                                    <p className="text-blue-600 font-bold text-2xl">
                                        {this.state.time_cotization} {this.state.days} {this.state.days_text}
                                    </p>
                                </div> : 
                                <div className="px-10">
                                    <h1 className="text-blue-500 text-lg my-3">Cotización realizada con exito</h1> 
                                </div>
                                
                            : 
                            <div className="px-10">
                                {
                                    this.state.isCompleted ? 
                                    <h1 className="text-blue-500 text-lg my-3">Cotización realizada con exito</h1> : 
                                    <h1 className="text-blue-500 text-lg my-3">Su cotización ha sido evaluada correctamente, aprete el botón "Entendido" para poder guardar sus datos y ponernos en contacto con usted.</h1>
                                }
                            </div>
                            
                        }
                        
                        <div className="w-full">
                            {
                                auth.isAuthenticatedSeller() ? 
                                    !this.state.isCompleted ? 
                                        <div className="flex flex-wrap -mx-3 mb-3 px-10">
                                            <div class="w-full px-3 mb-6 md:mb-0">
                                                <label class="block uppercase tracking-wide text-blue-700 font-bold text-xs font-bold mb-2" for="grid-first-name">
                                                    {this.state.offer_label}
                                                </label>
                                                <input onChange={(event)=>{
                                                    this.handleOffer(event)
                                                }} class="appearance-none block w-full bg-transparent text-white border border-gray-700 rounded-full text-xs py-2 px-4 mb-3 leading-tight focus:outline-none" id="grid-first-name" type="text"  placeholder={this.state.placeholder_offer}/>
                                            </div>
                                        </div> 
                                    : ''
                                :''
                            }
                        </div>
                        <div className="md:py-5 px-12 md:px-40">
                            {
                                !this.state.isCompleted ? 
                                    <button onClick={()=>{
                                        this.handleSaveCotization()
                                    }} className="py-1 bg-blue-500 focus:outline-none outline-none text-white w-full block rounded-full">{this.state.button_send}</button> 
                                :''
                            }
                            
                           {
                               this.state.isCompleted ? 
                               <button onClick={()=>{
                                this.props.resetHistoryOptions()
                                this.props.resetData() 
                                this.props.publicChangePage(0)
                            }} className="py-1 bg-blue-500 focus:outline-none outline-none text-white w-full block mt-8 rounded-full">{this.state.button_restart}</button> 
                            :''
                           }
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        page : state.page
    }
}
const dispatchStateToProps = {
    publicChangePage,
    resetHistoryOptions,
    resetData
}
export default connect(mapStateToProps, dispatchStateToProps)(Report) 