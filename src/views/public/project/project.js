import React, { Component } from 'react';
import Icon from '@material-ui/core/Icon'
import pic1 from '../../../assets/pic1.svg'
import pic2 from '../../../assets/pic2.svg'
import pic3 from '../../../assets/pic3.svg'
import pic4 from '../../../assets/pic4.svg'
import pic5 from '../../../assets/pic5.svg'
import Slide from 'react-reveal/Slide'
import global from 'api/global'
import auth from 'assets/auth'
import Cotization from './report'

import {connect} from 'react-redux'
import {changeProjectSection, changeText, resetHistoryOptions} from '../../../redux/actions/app.actions'
import axios from 'axios';

class Project extends Component{
    state={
        name_project:'',
        description_project:'',
        client_name : '',
        email:'',
        name_enterprise:'',
        enterprise_type:'',

        type_enterprises : [
            {
                name : 'Soy particular',
                pic:pic1
            },
            {
                name : 'Startup',
                pic:pic2
            },
            {
                name : 'Pyme',
                pic:pic3
            },
            {
                name : 'Agencia',
                pic:pic4
            },
            {
                name : 'Gran empresa',
                pic:pic5
            }
        ],

        projectIdCreated:''

        
    }

    renderProjectDetail = () => {
        return (
            <div className="w-full h-screen p-10">
                <div className="h-full pl-10 px-3">
                    <div className="pt-12">
                        <h1 className="text-2xl font-bold text-blue-600">Cuentanos más a detalle de tu proyecto </h1>
                    </div>
                    <div className="w-full flex flex-wrap -mx-3 py-10">
                        <div className="w-full md:w-full px-3 mb-6 md:mb-0">
                            <label className="block italic text-blue-600 font-bold py-3">
                                Nombre del proyecto
                            </label>
                            <input onChange={(event)=> this.setState({
                                name_project : event.target.value
                            })} placeholder="Este nombre nos servira para utilizarlo en todos los medios" className="w-full block bg-roots text-gray-400 md:w-1/2 border-b-2 py-3 focus:outline-none"/>
                        </div>
                        <div className="w-full md:w-full px-3 mb-6 md:mb-0">
                            <label className="block italic text-blue-600 font-bold py-3">
                                Descripcion tecnica del proyecto
                            </label>
                            <textarea onChange={(event)=> this.setState({
                                description_project: event.target.value
                            })} placeholder="Cuentanos que funcionalidades o requisitos tecnicos necesita tu proyecto" className="w-full text-gray-400 bg-roots block md:w-1/2 border-b-2 py-3 focus:outline-none"></textarea>
                        </div>
                        <div className="w-full flex justify-center py-10">
                            <button onClick={()=>{
                                this.changeProjectSection(1)
                            }} className="py-3 text-lg px-16 rounded shadow hover:font-bold outline-none focus:outline-none bg-gray-200 text-gray-600">Continuar</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

   

    renderClientForm = () =>{
        return(
            <Slide left>
                <div className="w-full h-full">
                        <h1 className="text-2xl my-5 font-bold text-blue-500">Para terminar, quisieramos saber más sobre ti!</h1>
                        <div className="w-full flex flex-wrap sm:flex sm:flex-wrap -mx-3 sm:-mx-3 py-10 sm:px-40 pb-10">
                            <div className="w-full md:w-1/2 px-3 mb-6 md:mb-2">
                                <label className="block uppercase tracking-wide text-gray-400 text-xs font-bold mb-2" for="grid-first-name">
                                    Nombres completos
                                </label>
                                <input onChange={(event)=>
                                this.setState({
                                    client_name : event.target.value
                                })
                                } className="appearance-none block w-full text-gray-500 border border-blue-600 rounded py-3 px-4 mb-3 bg-roots leading-tight focus:outline-none " id="grid-first-name" type="text" placeholder="Nombre completo"/>
                            </div>
                            <div className="w-full md:w-1/2 px-3 mb-6 md:mb-2">
                                <label className="block uppercase tracking-wide text-gray-400 text-xs font-bold mb-2" for="grid-first-name">
                                    Email
                                </label>
                                <input onChange={(event)=>
                                this.setState({
                                    email : event.target.value
                                })
                                } className="appearance-none block w-full text-gray-500 border border-blue-600 rounded py-3 px-4 mb-3 bg-roots leading-tight focus:outline-none" id="grid-first-name" type="text" placeholder="Correo electronico"/>
                            </div>
                            
                            <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                                <label className="block uppercase tracking-wide text-gray-400 text-xs font-bold mb-2" for="grid-first-name">
                                    Nombre de la empresa
                                </label>
                                <input onChange={(event)=> 
                                this.setState({
                                    name_enterprise : event.target.value
                                })
                                } className="appearance-none block w-full text-gray-500 border border-blue-600 rounded py-3 px-4 mb-3 bg-roots leading-tight focus:outline-none" id="grid-first-name" type="text" placeholder="Nombre de empresa"/>
                            </div>
                        </div>
                        
                        <div className="w-full">
                            <h1 className="text-lg my-5 text-blue-500">¿Que tipo de empresa eres?</h1>
                            <div className="flex w-full flex-wrap md:justify-center text-white uppercase mt-5">
                            {
                                this.state.type_enterprises.map((enterprise, index)=>{
                                    return (
                                        <div key={index} className="w-full md:w-1/6 p-3">
                                            <div onClick={()=>{
                                                this.setState({
                                                    enterprise_type:enterprise.name
                                                })
                                            }} className="bg-blue-500 py-2 hover:bg-blue-300 rounded">
                                                <img src={enterprise.pic} className="w-full h-20"/>
                                                <div className="py-4 flex justify-center">
                                                <h1>{enterprise.name}</h1>
                                                </div>
                                            </div>
                                        </div>
                                    )
                                })
                            }
                            </div>
                        </div>
                        <div className="w-full flex justify-center mt-2">
                        <button onClick={()=>{
                                this.handleSaveProject()
                                
                            }} className="py-3 text-lg px-16 rounded shadow hover:font-bold outline-none focus:outline-none bg-gray-200 text-gray-600">Guardar proyecto</button>
                        </div>
                    </div>
            </Slide>
        )
    }
    setSection(){
        switch (this.props.page.project_section){
            case 0 :
                return this.renderProjectDetail()
            case 1:
                return this.renderClientForm()
            case 2:
                return <Cotization project_created= {this.state.projectIdCreated} email = {this.state.email} enterprise = {this.state.name_enterprise}/>
            default :
                return this.renderProjectDetail()
        }
    }
    
   
    render(){
            return (
                <div className="w-full h-full">
                    <div className="mx-5 pb-1 md:mx-10 rounded">
                        {this.setSection()}
                    </div>
                </div>
            )
    }
    changeProjectSection = (index) => {
        this.props.changeProjectSection(index)
    }
    changeText = (text) => {
        this.props.changeText(text)
    }
    handleSaveProject = async() => {
        const {data} = await axios.post(global.BASE_URL+`proyecto`,{
            nombre_proyecto : this.state.name_project,
            comentarios:this.state.description_project,
            nombre_completo:this.state.client_name,
            email : this.state.email,
            nombre_empresa:this.state.name_enterprise,
            tipo_empresa:this.state.enterprise_type
        })

        if(data.status){
            this.setState({
                projectIdCreated : data.cotizacion._id
            })
            this.changeProjectSection(2)
        }
        else{
            auth.forcedLogOut()
        }
    }
}

const mapStateToProps = (state) => {
    return{
        page : state.page
    }
}

const dispatchStateToProps = {
    changeProjectSection,
    changeText,
    resetHistoryOptions,
}


export default connect(mapStateToProps, dispatchStateToProps)(Project)