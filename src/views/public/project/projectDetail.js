import React, { Component } from 'react';
import Slide from 'react-reveal/Slide'
class DetailProject extends Component{
    render(){
        return(
            <div className="w-full h-screen p-10">
                <div className="bg-white h-full pl-10 px-3">
                    <div className="pt-12">
                        <h1 className="text-2xl font-bold">Cuentanos más a detalle de tu proyecto </h1>
                    </div>
                    <div className="w-full flex flex-wrap -mx-3 py-10">
                        <div className="w-full md:w-full px-3 mb-6 md:mb-0">
                            <label className="block italic text-blue-600 font-bold py-3">
                                Nombre del proyecto
                            </label>
                            <input placeholder="Este nombre nos servira para utilizarlo en todos los medios" className="w-full block md:w-1/2 border-b-2 py-3 focus:outline-none"/>
                        </div>
                        <div className="w-full md:w-full px-3 mb-6 md:mb-0">
                            <label className="block italic text-blue-600 font-bold py-3">
                                Descripcion tecnica del proyecto
                            </label>
                            <textarea placeholder="Cuentanos que funcionalidades o requisitos tecnicos necesita tu proyecto" className="w-full block md:w-1/2 border-b-2 py-3 focus:outline-none"></textarea>
                        </div>
                        <div className="w-full flex justify-center py-10">
                            <button onClick={()=>{
                                this.props.changeProjectSection(1)
                            }} className="py-3 text-lg px-16 rounded shadow hover:font-bold outline-none focus:outline-none bg-gray-200 text-gray-600">Continuar</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default DetailProject