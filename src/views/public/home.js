import React, {Component} from 'react'

import iconHome from 'assets/homeIcon.png'
import NavBar from '../auth/seller/components/navbar'

import auth from 'assets/auth'

import lang from '../../translations/translate'

import {connect} from 'react-redux'
import {changeLang} from '../../redux/actions/app.actions'

import cookie from 'react-cookies'
import { googleTranslate } from '../../utils/googleTranslate'


class PublicHome extends Component {
    state={
        isLoggedIn : false,
        languageCodes : [],
        
        language: cookie.load("language") ? cookie.load("language") : "es",
        
        paragraph1: cookie.load("paragraph1")
        ? cookie.load("paragraph1")
        : "¿Cuanto cuesta desarollar mi app?",
        
        paragraph2: cookie.load("paragraph2")
        ? cookie.load("paragraph2")
        : "Calcula de forma rapida el coste para crear tu app, contestando estas sencillas preguntas",

        buttonHome:cookie.load("buttonHome")
        ? cookie.load("buttonHome")
        : "Calcular coste",

        welcome_user:'Hola',
        button_logout:'Cerrar sesion',
        dashboard: 'Dashboard'
    }

    componentDidMount(){
        cookie.remove('language');
        cookie.remove('title')
        cookie.remove("paragraph1")
        cookie.remove('paragraph2')
        cookie.remove('buttonHome')
        cookie.remove('option')
        cookie.remove('title-section')
        cookie.remove('title-option')
        
        if(auth.isAuthenticatedSeller()){
            this.setState({
                isLoggedIn : true
            })
        }

        const getLanguageCodes = languageCodes => {
            this.setState({ languageCodes });
        };

        googleTranslate.getSupportedLanguages("es", (err, languageCodes) =>{
            getLanguageCodes(languageCodes); // use a callback function to setState
        });
    
    }

    closeSession =() => {
        this.setState({
            isLoggedIn : false
        })
        auth.logoutSeller()
    }

   

    questionHandler = (language) => {
        let { paragraph1, paragraph2, buttonHome, welcome_user, button_logout, dashboard } = this.state;
        
        let cookieLanguage = cookie.load("language");
        let transQuestion = "";
    
        const translating_phrase1 = transQuestion => {
          if (paragraph1 !== transQuestion) {
            this.setState({ paragraph1: transQuestion });
            cookie.save("question", transQuestion, { path: "/" });
          }
        };
    
        if (language !== cookieLanguage) {
          googleTranslate.translate(paragraph1, language, function(err, translation) {
            transQuestion = translation.translatedText;
            translating_phrase1(transQuestion);
          });
        }

        const translating_phrase2 = transQuestion => {
            if (paragraph2 !== transQuestion) {
              this.setState({ paragraph2: transQuestion });
              cookie.save("question", transQuestion, { path: "/" });
            }
          };
      
          // translate the question when selecting a different language
          if (language !== cookieLanguage) {
            googleTranslate.translate(paragraph2, language, function(err, translation) {
              transQuestion = translation.translatedText;
              translating_phrase2(transQuestion);
            });
          }

          const translating_phrase3 = transQuestion => {
            if (buttonHome !== transQuestion) {
              this.setState({ buttonHome: transQuestion });
              cookie.save("question", transQuestion, { path: "/" });
            }
          };
      
          // translate the question when selecting a different language
          if (language !== cookieLanguage) {
            googleTranslate.translate(buttonHome, language, function(err, translation) {
              transQuestion = translation.translatedText;
              translating_phrase3(transQuestion);
            });
          }
        
          const translating_phrase = transQuestion => {
            if (welcome_user !== transQuestion) {
              this.setState({ welcome_user: transQuestion });
              cookie.save("question", transQuestion, { path: "/" });
            }
          };
      
          // translate the question when selecting a different language
          if (language !== cookieLanguage) {
              googleTranslate.translate(welcome_user, language, function(err, translation) {
                transQuestion = translation.translatedText;
                translating_phrase(transQuestion);
              });
          }

          const translating_buttonlogout = transQuestion => {
            if (button_logout !== transQuestion) {
              this.setState({ button_logout: transQuestion });
              
            }
          };
      
          // translate the question when selecting a different language
          if (language !== cookieLanguage) {
              googleTranslate.translate(button_logout, language, function(err, translation) {
                transQuestion = translation.translatedText;
                translating_buttonlogout(transQuestion);
              });
          }

          const translating_buttondashboard = transQuestion => {
            if (dashboard !== transQuestion) {
              this.setState({ dashboard: transQuestion });
              
            }
          };
      
          // translate the question when selecting a different language
          if (language !== cookieLanguage) {
              googleTranslate.translate(dashboard, language, function(err, translation) {
                transQuestion = translation.translatedText;
                translating_buttondashboard(transQuestion);
              });
          }

        this.setState({ language });
        cookie.save("language", language, { path: "/" });

    }
   
    render(){
        
        const { languageCodes, language, paragraph1, paragraph2, buttonHome } = this.state;
        const user = JSON.parse( localStorage.getItem('session-seller') )
        return (
        <div className="w-full h-full">
           <NavBar questionHandler={this.questionHandler} languageCodes={languageCodes} language={language} changeLang={this.changeLang} lang={this.props.page.lang} logout_text={this.state.button_logout} closeSession={this.closeSession} isLoggedIn={this.state.isLoggedIn} dashboard_button={this.state.dashboard}/>
            <div className="w-full h-full">
                <div className="flex flex-wrap justify-center w-full">
                    <img src={iconHome} className="h-48 mt-10"/>
                    <div className="w-full text-center py-5 text-white">
                        {
                            this.state.isLoggedIn ?  <p className="pb-5 font-black text-4xl"> {this.state.welcome_user} {user.name}!</p> : ''
                        }
                        <p className="pb-5 font-black text-4xl">{paragraph1}</p>
                        <p>{paragraph2}</p>
                    </div>
                    <div className="w-full flex justify-center">
                    <button onClick={ ()=> this.props.publicChangePage(this.props.services) } className="py-1 px-24 outline-none focus:outline-none font-bold rounded-full bg-blue-500 text-white uppercase">{ buttonHome }</button>
                    </div>
                </div>
            </div>
        </div>
        )
    }

}
const mapStateToProps = (state) => {
    return {
        page : state.page
    }
}
const dispatchStateToProps = {
    changeLang
}
export default connect(mapStateToProps, dispatchStateToProps)(PublicHome)