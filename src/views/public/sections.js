import React, {Component} from 'react'
import logo from 'assets/logoroots.png'
import SectionCard from 'components/sectionCard'
import axios from 'axios'
import global from 'api/global'
import {connect} from 'react-redux'
import Controls from 'components/controls'
import BackSection from 'components/backButtonSection'
import {publicChangePage, fetchSections, addDataPublic, nextSection, lastSection} from '../../redux/actions/app.actions'
import Section from './template/section'
import CircularProgress from '@material-ui/core/CircularProgress'
import Report from './project/report'
import NavBar from 'components/navBarSections'
import Project from './project/project'

import cookie from 'react-cookies'
import { googleTranslate } from '../../utils/googleTranslate'

class Sections extends Component{
    
    state={
        sections:[],
        data:[],
        loading:true,
        noMoreSections : false,
        title : ''
    }
    translate = text => {
        let language = cookie.load("language");

        const traduce = parameter => {
           this.setState({
                title : parameter
           }) 
        }
        googleTranslate.translate(text, language, function(err, translation) {
            traduce(translation.translatedText)
        });

        
    }
    async componentDidMount(){
        try {
            
            //parametro del servicio
            const { parameter_selected } = this.props.page

            //recolectar las secciones del servicio
            const { data } = await axios.get(global.BASE_URL+`servicios-public/${parameter_selected}`)
            
            this.setState({
                sections : data.servicios.secciones,
                loading:false
            })     

            //si ya no hay secciones no seguira dando data
            if(this.state.sections[this.props.page.index_section]){
                this.setState({ data: this.state.sections[this.props.page.index_section]})
            }

            this.translate(this.state.sections[this.props.page.section].nombre)

        } catch (error) {
            console.log(error)
        }
    }

    changeText = text => {
        this.setState({title: text})
        this.translate(text)
    }
   
    addDataPublic = data => {
        this.props.addDataPublic(data)
    }

    nextSection = async() => {
        this.props.nextSection()
    }

    renderOptions = (option, index) => {

        if(cookie.load('language')){
            let language = cookie.load("language") ? cookie.load('language') : '';
            let transQuestion = "";

            const translating_phrase1 = transQuestion => {
                option.nombre = transQuestion
            };

            googleTranslate.translate(option.nombre, language, function(err, translation) {
                transQuestion = translation.translatedText;
                translating_phrase1(transQuestion);
            });

            cookie.save("language", language, { path: "/" });
        }

        if( this.state.sections.length > 1){
            return (
                <div key={index} onClick={()=> { 
                            this.nextSection()
                            
                            var object = {
                                section : this.state.sections[this.props.page.section]._id,
                                option : option
                            }
                            this.addDataPublic(object)

                            this.setState({title:''})
                        }
                    } className="w-full sm:w-full md:w-1/3 lg:w-1/3 p-2">
                    <div className="py-10 rounded text-center m-5 hover:bg-gray-600 hover:text-white p-2 transition duration-500 ease-in-out transform hover:-translate-y-1 hover:scale-110 text-blue-600">
                        <div className="flex justify-center">
                            <img className="h-32" src={option.icono} />
                        </div>
                        <p>{option.nombre}</p>
                    </div>
                </div>
            )
            
        }
        else{

            return (
                <div key={index} onClick={ ()=>{
                    this.nextSection()
                    
                    var object = {
                        section : this.state.sections[this.props.page.section]._id,
                        option : option
                    }
                    this.addDataPublic(object)

                    this.setState({title:''})
                    
                } } className="w-full sm:w-full md:w-1/3 lg:w-1/3 p-2">
                    <div className="py-10 rounded hover:bg-gray-600 m-5 hover:text-white text-center p-2 transition duration-500 ease-in-out transform hover:-translate-y-1 hover:scale-110 text-blue-600">
                        <div className="flex justify-center">
                        <img className="h-32" src={option.icono} />
                        </div>
                        <p>{option.nombre}</p>
                    </div>
                </div>
            )
        }
    }

    lastSection = () => {
        this.props.lastSection()
    }

    noSections = () => {
        return (
            <h1>No hay secciones de este servicio disponibles</h1>
        )
    }

    render(){
        if(this.state.loading){
           return (
                <div className="w-full h=hull flex justify-center mt-40">
                    <CircularProgress color="secondary"/>
                </div>
           )
        }
        else{
            if(this.state.sections[this.props.page.section]) 
            return (
                <div className="w-full w-full">
                    {
                        this.state.sections[this.props.page.section] ? <NavBar data={this.state} id={this.props.page.section}/> : ''
                    }

                    <div className="w-full px-4 pt-10 sm:pt-0 md:pt-20 lg:pt-20 flex justify-center">
                        <h1 className="text-2xl text-blue-500"> {this.state.title ? this.state.title : this.translate(this.state.sections[this.props.page.section].nombre) }</h1>
                    </div>
                    <div className="flex flex-wrap py-4 sm:flex sm:flex-wrap md:mx-64 sm:mx-0">
                    {
                        this.state.sections[this.props.page.section].opciones ?
                        this.state.sections[this.props.page.section].opciones.map((options, index)=>{
                            return this.renderOptions(options, index)
                        }) : ''
                    }
                    </div>
                    {
                
                        this.props.page.section == 0 ? (
                            <Controls lastSection={this.lastSection} number={1}/>
                        ) : 
                        (
                            this.state.sections[this.props.page.section] ? 
                            <BackSection lastSection = { this.lastSection } /> : ''   
                        )
                        
                    }
                </div>
        )
        else{
            return <Project />
        }}
    }
}

const mapStateToProps = (state) => {
    return {
        page : state.page
    }
}

const dispatchStateToProps = {
    publicChangePage,
    fetchSections,
    addDataPublic,
    nextSection,
    lastSection
}


export default connect(mapStateToProps,dispatchStateToProps)(Sections)

