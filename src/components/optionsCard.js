import React, {Component} from 'react'
import { publicChangePage } from '../redux/actions/app.actions'
import { connect } from 'react-redux'
import global from 'api/global'
class OptionCard extends Component{
    
    state={

    }

    render(){
        const { nombre, _id, icono } = this.props
        return(
            <div className="w-full md:w-1/3 p-2">
                <div>
                    <img src={ global.BASE_URL+icono }/>
                </div>
                <div onClick={ ()=> this.props.publicChangePage(this.props.number, _id) } className="bg-white shadow-xl hover:bg-blue-400 hover:text-white rounded hover:shadow-2xl py-4 text-center text-blue-600 p-2 cursor-default">
                    <a> { nombre }  </a>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        page : state.page
    }
}

const dispatchStateToProps = {
    publicChangePage
}

export default connect(mapStateToProps,dispatchStateToProps)(OptionCard)