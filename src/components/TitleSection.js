import React, {Component} from 'react'
import cookie from 'react-cookies'
import { googleTranslate } from '../../utils/googleTranslate'

class TitleSection extends Component{
    
    render(){
            
        return <h1 className="text-2xl text-blue-500"> {this.props.title}</h1>
    }
}
export default TitleSection