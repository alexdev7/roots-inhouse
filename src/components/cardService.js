import React, {Component} from 'react'

class CardService extends Component{
    componentDidMount(){
    }
    state={

    }

    render(){
        const section = this.props.sections
        return(
            <div className="my-1 px-1 w-full md:w-1/2 lg:my-4 lg:px-4 lg:w-1/3">
                <div className="overflow-hidden hover:shadow-2xl bg-white rounded-lg shadow-lg">
                    <a href="#">
                        <img alt="Placeholder" className="block h-auto w-full" src={ section[0] }/>
                    </a>
                    <div className="flex items-center justify-center leading-tight p-2 md:p-4">
                        <h1 className="text-lg">
                            <a className="no-underline hover:underline text-black" href="#">
                                { section[0].nombre }
                            </a>
                        </h1>
                    </div>
                </div>
            </div> 
            
        )
    }
}

export default CardService