import React, {Component} from 'react'

import Icon from '@material-ui/core/Icon'

class IconComponent extends Component{
    
    render(){
        const classes = this.props.className
        return (
            <Icon className={classes} >{ this.props.IconName }</Icon>
        )
    }
}

export default IconComponent