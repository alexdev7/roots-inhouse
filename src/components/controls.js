import React , {Component } from 'react'
import Icon from '@material-ui/core/Icon'

import {publicChangePage} from '../redux/actions/app.actions'
import { connect } from 'react-redux'

class Controls extends Component{
    render(){
        return (
            <div className="flex flex-wrap md:px-64 mt-12">
                <div className="w-full flex justify-center px-32 p-2">
                    <button onClick={ ()=> {
                        this.props.publicChangePage(this.props.number)
                    } } className="bg-transparent shadow-xl flex justify-center border border-blue-500 rounded hover:shadow-2xl focus:outline-none py-4 px-4 text-center text-blue-500 p-2 cursor-default">
                        <Icon className="">arrow_back</Icon>
                    </button>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        page : state.page
    }
}

const dispatchStateToProps = {
    publicChangePage
}

export default connect(mapStateToProps,dispatchStateToProps)(Controls)