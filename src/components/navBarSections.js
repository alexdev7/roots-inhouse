import React, { Component } from 'react';
class NavBarSection extends Component{
    render(){
        
        if(this.props.data.sections[this.props.id]){
            return (
                <div className="pt-10">
                    <div className="flex justify-center text-blue-500 font-bold text-2xl">
                        {
                            this.props.id == this.props.data.sections.length ? '' :
                            <h1> { `${this.props.id+1}/${this.props.data.sections.length}` } </h1> 
                        }
                        
                    </div>
                    
                </div>
            )
        }
        else{
            return ''
        }
    }
}

export default NavBarSection