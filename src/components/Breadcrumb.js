import React, {Component} from 'react'

class BreadcrumbComponent extends Component {
    
    render(){
        return (
            <nav className="bg-gray-400 p-3 rounded font-sans w-full">
                <ol className="list-reset flex text-grey-dark">
                    {this.props.children}
                    {
                        this.props.child ?
                        <>
                            <li>
                                <span className="mx-2">/</span>
                            </li>
                            <li>
                                <a href="#" className="text-blue font-bold">{this.props.child}</a>
                            </li>
                        </>
                        :''
                    }
                </ol>
            </nav>
        )
    }
}

export default BreadcrumbComponent