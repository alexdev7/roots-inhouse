import React,  {Component} from 'react'
import Icon from '@material-ui/core/Icon'
class ButtonBackSection extends Component {
    constructor(props){
        super(props)
    }
    render(){
        
        return (
            <div className="flex flex-wrap md:px-64 mt-12">
                <div className="w-full flex justify-center px-32 p-2">
                    <button onClick={ ()=> this.props.lastSection() } className="bg-white shadow-xl flex justify-center hover:bg-blue-400 hover:text-white rounded hover:shadow-2xl focus:outline-none py-4 px-4 text-center text-blue-600 p-2 cursor-default">
                        <Icon className="">arrow_back</Icon>
                    </button>
                </div>
            </div>
        )
    }
}

export default ButtonBackSection