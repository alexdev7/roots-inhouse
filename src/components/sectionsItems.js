import React, {Component} from 'react'

class SectionItems extends Component{

    render(){
        return (
            <div className="py-1 pl-4 text-gray-600 mt-3">
                { this.props.sectionName }
            </div>
        )
    }
}
export default SectionItems
