import React, {Component} from 'react'
import Section from '../components/sectionsItems'
import Item from '../components/sideNavItem'
import auth from 'assets/auth'

import { changePage } from '../redux/actions/app.actions'

import { connect } from 'react-redux'

import HomeOutlinedIcon from '@material-ui/icons/HomeOutlined';
import AccountCircleOutlinedIcon from '@material-ui/icons/AccountCircleOutlined';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import SupervisroAccount from '@material-ui/icons/SupervisorAccount'
import Build from '@material-ui/icons/BuildOutlined'
import VerticalSplit from '@material-ui/icons/VerticalSplit'
import Cotizations from '@material-ui/icons/ImportContacts'
import MarkRequests from '@material-ui/icons/MarkunreadOutlined'

class SideNav extends Component{
    constructor(props){
        super(props)
        this.handleChangePage = this.handleChangePage.bind(this);
    }
    
    handleChangePage(number){
        this.props.changePage(number)
    } 

    renderDashboardsItems =()=> {
        if(!auth.isAuthenticatedSeller()){
            return (
            <>
                    
                    <Item handleChangePage = {this.handleChangePage} number = {3} ItemName="Secciones">
                        <VerticalSplit className="text-black"/>
                    </Item>
                    <Item iconItem="scatter_plot" handleChangePage = {this.handleChangePage} number = {4} ItemName="Servicios">
                        <Build  className="text-black"/>
                    </Item>
                    <Item iconItem="assignment_ind" handleChangePage={this.handleChangePage} number={5} ItemName="Vendedores">
                        <SupervisroAccount className="text-black"/>
                    </Item>
                    <Item iconItem="chrome_reader_mode" handleChangePage={this.handleChangePage} number={6} ItemName="Cotizaciones">
                        <Cotizations className="text-black"/>
                    </Item>
                    <Item iconItem="chrome_reader_mode" handleChangePage={this.handleChangePage} number={7} ItemName="Solicitudes">
                        <MarkRequests  className="text-black"/>
                    </Item>
                    
            </>
            )
        }
        else{
            return(
                <>
                    <Section sectionName="Secciones"/>
                    <Item iconItem="chrome_reader_mode" handleChangePage={this.handleChangePage} number={6} ItemName="Cotizaciones"/>
                </>
            )
        }
    }

    render(){
        return (
         
            <div className="bg-white p-0 w-0 sm:p-0 sm:w-0 md:p-6 md:w-64 lg:p-6 lg:w-64 shadow-2xl">
                <Section sectionName="General"/>
                <ul>
                    <Item iconItem="dashboard" handleChangePage = { this.handleChangePage } number = {0} ItemName="Dashboard">
                        <HomeOutlinedIcon className="text-black"/>
                    </Item>
                    <Item iconItem="person" handleChangePage = { this.handleChangePage } number = {1} ItemName="Mi perfil">
                        <AccountCircleOutlinedIcon className="text-black"/>
                    </Item>
                    <Item iconItem="person" handleChangePage = { this.handleChangePage } number = {2} ItemName="Seguridad">
                        <LockOutlinedIcon className="text-black"/>
                    </Item>
                </ul>
                { auth.isAuthenticatedSeller() ? '' : <Section sectionName="Mantenimiento"/> }
                <ul className="overflow-auto h-56">
                    {this.renderDashboardsItems()}
                </ul>
                
            </div>
                    
        )
    }
}

const dispatchStateToProps = {
    changePage
}



export default connect(null, dispatchStateToProps)(SideNav)