import React, { Component } from 'react';
import Slider from 'react-reveal/Slide'
class CircleSection extends Component {
    render(){
        return(
            <Slider left>
                 <span className="bg-blue-500 text-blue-500 h-2 w-2 m-2 px-2 rounded-full"></span>
            </Slider>
        )
    }
}

export default CircleSection