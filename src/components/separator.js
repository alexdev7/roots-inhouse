import React, {Component} from 'react'

class Separator extends Component {
    render(){
        return (
            <li>
                <span className="mx-2">/</span>
            </li>
        )
    }
}

export default Separator