import React, { Component } from 'react';
import CircleSectionIndicator from './circleSection'
import Icon from './IconComponent'

import {connect} from 'react-redux'
import {changeProjectSection} from '../redux/actions/app.actions'
class FooterProject extends Component {
    
    render(){
        return (
        <div className="pt-20">
            <div className="w-full flex justify-center">
                <div className="flex justify-between">
                        {
                            this.props.page.project_section == 0 ? '' : 
                            <button onClick={()=>
                                this.props.setText(this.props.page.text,0)
                            } className="text-gray-700 disabled:opacity-50 text-center bg-blue-400 px-2 py-2 m-5 focus:outline-none outline-none rounded-full flex justify-center "> <Icon IconName="keyboard_arrow_left"/> </button>
                        }
                        <div className="pt-8">  
                            <CircleSectionIndicator />
                            <CircleSectionIndicator />
                            <CircleSectionIndicator />
                        </div>
                        {
                            this.props.page.text ? 
                            <button className="text-gray-700 text-center bg-blue-400 px-2 py-2 m-5 rounded-full flex focus:outline-none outline-none justify-center "> <Icon IconName="keyboard_arrow_right"/> </button> :
                            ''

                        }
                </div>
            </div>
        </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        page : state.page 
    }
}
const dispatchStateToProps = {
    changeProjectSection
}
export default connect(mapStateToProps, dispatchStateToProps)(FooterProject)