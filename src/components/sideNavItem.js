import React, {Component} from 'react'

class ItemNav extends Component{
    constructor(props)
    {
        super(props)
    }
    render(){
        return (
            <li>
                <a onClick = { () => this.props.handleChangePage(this.props.number) } className="flex items-center border-blue-500 border-l-2 py-3 my-2 hover:bg-gray-200 hover:text-blue-500">
                    <div className="py-2 px-2 h-10 rounded hover:text-blue-500">
                        {
                            this.props.children
                        }
                    </div>
                    <span className="ml-3 cursor-default text-lg">{ this.props.ItemName }</span>
                </a>
            </li>
        )
    }
}
export default ItemNav
