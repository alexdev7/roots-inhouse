import React, {Component} from 'react'
import logo from '../assets/whitelogo.png'
import auth from '../assets/auth'
import Icon from '@material-ui/core/Icon'
class NavBar extends Component{
    
    state = {
        user : ''
    }
    componentDidMount(){
        const user = JSON.parse( localStorage.getItem('session') ) || JSON.parse( localStorage.getItem('session-seller') )
        this.setState({
            user : user.user
        })
    }
    closeSession (){
       auth.logout()
       window.location.href="/login"
    }
    render(){
        return(
            <div className="items-center bg-white flex px-4 py-2 justify-between">
                <div className="bg-blue-500 ml-6 rounded shadow px-2">   
                    <img className="h-10" src={ logo }/>
                </div>
                <div className="text-white">
                    <div className="px-1 py-1 items-center">
                        {
                            auth.isAuthenticatedSeller() ? 
                            <>
                                <button onClick={()=>{
                                    window.location.href = "/"
                                }} 
                                className="px-3 py-1 rounded border font-bold border-white text-black">Inicio</button>
                                <button onClick={()=>this.closeSession()} className="px-3 ml-2 py-1 font-bold rounded border border-white text-black">Cerrar sesión</button>
                            </> : 
                            <div>
                                <a className="px-3 py-1 font-bold text-black">{this.state.user}</a>
                                <button onClick={()=>this.closeSession()} className="px-3 py-1 focus:outline-none outline-none font-bold text-black">Cerrar sesión</button>
                            </div>

                        }
                    </div>
                </div>
            </div>
        )
    }
}

export default NavBar