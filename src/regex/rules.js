export default {
    empty:/^(\w+\S+)/,
    string : /^[a-zA-Z\s]*$/,
    number:/^[0-9]*$/,
    validEmail : /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
}