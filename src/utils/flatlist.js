import React, {Component} from 'react'

class FlatList extends Component{
    state={
        count:0
    }
    componentDidMount(){
        console.log(this.props.data)
    }

    render(){
        return(
            <div>
            {
                this.props.data.map(()=>{
                    {this.props.render}
                })
            }
            </div>
        )
    }
}

export default FlatList