import React , { useState } from 'react';

//router 
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

//views
import Home from './views/public'
import Login from './views/auth/login'
import Dashboard from './views/auth/dashboard'
import Sections from './views/auth/sections'
import {Private} from './private/privateComponent'
import { Provider } from 'react-redux'
import store from './redux/store'

function App() {
  return (
      <Provider store={ store }>
            <Router>
              <Switch>
                <Route path="/" component = { Home } exact/>
                <Route path="/login" component = { Login } exact/>
                <Private path="/private/home" component = { Dashboard } exact/>
                <Private path="/private/sections" component = { Sections } exact/>
              </Switch>
            </Router>
      </Provider>
  );
}

export default App;
