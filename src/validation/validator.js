import regex from '../regex/rules'
const test = (value, rule) => {
    const regex = new RegExp(rule)

    return regex.test(value)
}
export default { 
    isEmpty(value){
        if(value == ''){
            return false
        }
        else{
            return true 
        }
    },
    isValid(value, type){
        if(type === 'string'){
            return test(value, regex.string)
        }
        else if(type === 'number'){
            return test(value, regex.number)
        }
    },
    validateEmail(value){
        if( test(value, regex.validEmail) ){
            return true
        }
        else{
            return false
        }
        
    }
    
}