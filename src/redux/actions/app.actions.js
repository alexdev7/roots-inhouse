export const CHANGE_PAGE = 'CHANGE_PAGE'

export const CHANGE_SECTION_OPTIONS = 'CHANGE_SECTION_OPTIONS'

export const CHANGE_SECTION_SECTIONS = 'CHANGE_SECTION_SECTIONS'

export const CHANGE_SECTION_SERVICES = 'CHANGE_SECTION_SERVICES'

export const PUBLIC_CHANGE_PAGE = 'PUBLIC_CHANGE_PAGE'

export const FETCH_SECTIONS = 'FETCH_SECTIONS'

export const ADD_DATA_PUBLIC = 'ADD_DATA_PUBLIC'

export const NEXT_SECTION = 'NEXT_SECTION'

export const LAST_SECTION = 'LAST_SECTION'

export const CHANGE_PROJECT_SECTION = 'CHANGE_PROJECT_SECTION'

export const CHANGE_TEXT = 'CHANGE_TEXT'

export const RESET_SECTIONS_OPTIONS = 'RESET_SECTIONS_OPTIONS'

export const RESET_DATA = 'RESET_DATA'

export const CHANGE_LANG = 'CHANGE_LANG'

export const CHANGE_SELLERS_SECTION = 'CHANGE_SELLERS_SECTION'

export const CHANGE_COTIZATION_VIEW = 'CHANGE_COTIZATION_VIEW'

export const CHANGE_PROJECT_VIEW = 'CHANGE_PROJECT_VIEW'
//pages
export const changePage = (index) => {
    return {
        type : CHANGE_PAGE,
        payload : index
    }
}

//options page sections
export const changeSectionOptions = (index) => {
    return {
        type : CHANGE_SECTION_OPTIONS,
        payload : index
    }
}

//section page sections
export const changeSectionOfSections = (index, parameter = {}) => {
    return {
        type : CHANGE_SECTION_SECTIONS,
        payload : {
            index, parameter
        }
    }
}

//services page sections 
export const changeServicesSections = (index, parameter = {}, data={}) => {
    return {
        type : CHANGE_SECTION_SERVICES,
        payload : {
            index, parameter, data
        }
    }
}

//public 
export const publicChangePage = (index, parameter) => {
    return {
        type : PUBLIC_CHANGE_PAGE,
        payload:{ 
            index, parameter
        }
    }
}

//fetch options for service selected 
export const fetchSections = index => {
    return {
        type:FETCH_SECTIONS,
        payload:index
    }
}
//store selection
export const addDataPublic = data => {
    return {
        type : ADD_DATA_PUBLIC,
        payload : data
    }
}

export const nextSection = (title) => {
    
    return {
        type : NEXT_SECTION,
        payload : ''
    }
}
export const lastSection = () => {
    return {
        type : LAST_SECTION,
        payload : ''
    }
}
export const changeProjectSection = (index) => {
    return{
        type: CHANGE_PROJECT_SECTION,
        payload : index
    }
}
export const changeText = (text) => {
    return{
        type : CHANGE_TEXT,
        payload : text
    }
}
export const resetHistoryOptions = () => {
    return {
        type : RESET_SECTIONS_OPTIONS,
        payload:''
    }
}
export const resetData = () => {
    return {
        type : RESET_DATA,
        payload:''
    }
}

export const changeLang = (lang) => {
    return {
        type : CHANGE_LANG,
        payload : lang
    }
}

export const changeSellerSection = (index) => {
    return {
        type:CHANGE_SELLERS_SECTION,
        payload : index
    }
}

export const changeCotizationView = (index, parameter=null) => {
    return {
        type:CHANGE_COTIZATION_VIEW,
        payload:{
            index, parameter
        }
    }
}

export const changeProjectView = (index, parameter=null) => {
    return {
        type:CHANGE_PROJECT_VIEW,
        payload:{
            index, parameter
        }
    }
}


