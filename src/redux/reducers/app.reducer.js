import { 
    CHANGE_PAGE, 
    CHANGE_SECTION_OPTIONS, 
    CHANGE_SECTION_SECTIONS, 
    PUBLIC_CHANGE_PAGE, 
    FETCH_OPTIONS_SERVICE_SELECTED,
    CHANGE_SECTION_SERVICES,
    FETCH_SECTIONS,
    ADD_DATA_PUBLIC,
    NEXT_SECTION,
    LAST_SECTION,
    CHANGE_PROJECT_SECTION,
    CHANGE_TEXT,
    RESET_SECTIONS_OPTIONS,
    RESET_DATA,
    CHANGE_LANG,
    CHANGE_SELLERS_SECTION,
    CHANGE_COTIZATION_VIEW,
    CHANGE_PROJECT_VIEW
} from '../actions/app.actions'
import cookie from 'react-cookies'
var data_public =[]


const config = {
    init_page : 0,
    
    options : 0,
    sections : 0,
    services : 0,
    sellers:0,
    cotization:0,

    init_public_page : 0,

    parameter_selected: 0,

    parameter_cotization:'',

    parameter_service:0,

    parameter_section:0,
    edit_info:'',

    index_section : 0,

    lang:'es',

    data : '',

    section : 0,
    next : 1,
    last : 1,

    project_section : 0,

    project_view : 0,

    parameter_project:'',

    text:''
}

function Reducer( state = config, { type, payload } ){

    switch (type){
        case CHANGE_PAGE :
            return {...state, init_page: payload}

        case CHANGE_SECTION_OPTIONS:
            return {...state, options: payload}

        case CHANGE_SECTION_SECTIONS:
            return {...state, parameter_section : payload.parameter, sections: payload.index}

        case PUBLIC_CHANGE_PAGE:
            return {...state, parameter_selected : payload.parameter , init_public_page: payload.index }

        case CHANGE_SECTION_SERVICES :
            return {...state, parameter_service : payload.parameter, edit_info:payload.data, services: payload.index }

        case FETCH_SECTIONS : 
            return {...state, index_section : payload }
        case ADD_DATA_PUBLIC :
            if(payload.service){
                data_public.push({
                    service : payload.service,
                })
            }
            else{
                data_public.push({
                    sections : payload
                })

            }
            return {...state, data : data_public}
        case NEXT_SECTION : 
            var actualSection = state.section
            var next = actualSection + 1
           
            return {
                ...state, section : next
            }
        case LAST_SECTION:
            var actualSection = state.section
            var last = actualSection - 1
            
            data_public.pop()
            
            return {
                ...state, section : last, data : data_public
            }
        case CHANGE_PROJECT_SECTION : 
            return {...state, project_section : payload}

        case CHANGE_TEXT : 
            var lastText = state.text
            lastText = lastText +' '+payload
            return {...state, text : lastText}

        case RESET_SECTIONS_OPTIONS:
            return { ...state, section:0 }

        case RESET_DATA:
            data_public = []
            return { ...state, data : data_public }
        case CHANGE_LANG :
            return {...state, lang:payload}

        case CHANGE_SELLERS_SECTION:
            return {...state, sellers:payload}
        case CHANGE_COTIZATION_VIEW:
            return {...state, cotization:payload.index, parameter_cotization:payload.parameter}
        case CHANGE_PROJECT_VIEW : 
            return {...state, project_view:payload.index, parameter_project:payload.parameter}
        default :
            return state
    }
}

export default Reducer

