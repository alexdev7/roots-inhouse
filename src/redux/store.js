import { combineReducers, createStore } from 'redux'

import Reducer from './reducers/app.reducer' 

const store = createStore(combineReducers({ page : Reducer }), window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__())

export default store