export default {
    required: 'Este campo es requerido',
    priceInvalid : 'Formato invalido de dinero',
    imageRequired:'Seleccion de imagen requerida',
    imageSizeInvalid:'Las medidas de la imagen son invalidad (ej:500x500)'
    
}