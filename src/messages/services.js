export default {
    required : 'Este campo es requerido',
    onlyLetters: 'Este campo requiere solo letras',
    onlyNumbers: 'Este campo requiere solo numeros',
    selectedRequired : 'Seleccione un campo porfavor'
}