module.exports = {
    theme: {
      
      extend: {
        borderRadius:{
          rounded_roots_sm : '70px' ,
          rounded_roots_md : '74px',
          rounded_roots_lg : '76px'
        },
        fontWeight:{
          big_bold_text : '1000'
        },
        colors: {
          roots: '#282828',
          
        },
        container: {
          center: true,
        },
      }
    },
    variants: {
      opacity:['disabled']
    },
    
  }